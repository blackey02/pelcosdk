﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LiveVideo.Common;

namespace LiveVideo
{
    /// <summary>
    /// Main form used to get and view video
    /// </summary>
    public partial class LiveVideoForm : Form
    {
        // These constants represent the images in the imagelist
        private const int ONLINE_FIXED = 0;
        private const int OFFLINE_FIXED = 1;
        private const int ONLINE_PTZ = 2;
        private const int OFFLINE_PTZ = 3;

        // Member vairables
        private IVideoProvider _videoProvider;
        private List<ICamera> _cams = new List<Common.ICamera>();
        private List<string> _ids = new List<string>();
        private Panel _selectedPanel;

        /// <summary>
        /// Ctor
        /// </summary>
        public LiveVideoForm()
        {
            InitializeComponent();

            textBoxIp.Text = "10.220.198.146";
            textBoxPort.Text = "60001";
        }

        /// <summary>
        /// Provides the stream id of the current stream on the selected panel
        /// </summary>
        private string SelectedId
        {
            get
            {
                string id = string.Empty;
                if(_selectedPanel != null)
                    id = SessionFromPanel(_selectedPanel);
                return id;
            }
        }

        /// <summary>
        /// Event handler for when the form is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LiveVideoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Disconnect();
        }

        /// <summary>
        /// Event handler for when the form is resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LiveVideoForm_Resize(object sender, EventArgs e)
        {
            if (_videoProvider != null)
                _videoProvider.Resize();
        }

        /// <summary>
        /// Event handler for when the camera item has begun to be dragged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewCameras_ItemDrag(object sender, ItemDragEventArgs e)
        {
            listViewCameras.DoDragDrop(e.Item, DragDropEffects.Move);
        }

        /// <summary>
        /// Event handler for when the camera item has been dragged over a panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        /// <summary>
        /// Event handler for when the camera item has been dropped on a panel 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_DragDrop(object sender, DragEventArgs e)
        {
            Panel panel = (Panel)sender;
            ListViewItem item = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
            ConnectPlay(item, panel);
        }

        /// <summary>
        /// Event handler for when the login button has been pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            string ip = textBoxIp.Text;
            string username = textBoxUsername.Text;
            string password = textBoxPassword.Text;
            ushort port;
            bool parseSuccess = ushort.TryParse(textBoxPort.Text, out port);

            if (!parseSuccess)
                return;

            // Determine what type of provider we would like to create
            VideoProviderType providerType = VideoProviderType.Unknown;
            if (radioButtonObjectModel.Checked)
                providerType = checkBoxGetFrames.Checked ? VideoProviderType.ObjectVideoFrames : VideoProviderType.ObjectVideo;
            else if (radioButtonApiViewer.Checked)
                providerType = VideoProviderType.ApiVideo;
            else if (radioButtonOcx.Checked)
                providerType = VideoProviderType.OcxVideo;
            else
                return;

            Disconnect();

            buttonLogin.Enabled = false;

            // Create provider, get cameras asynchronously
            var isEdge = checkBoxOnEdge.Checked;
            var isRtsp = checkBoxUseRtsp.Checked;
            var decodedFrames = checkBoxDecoded.Checked;
            System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(obj =>
            {
                _videoProvider = VideoProviderFactory.CreateVideoProvider(providerType, isEdge, isRtsp, decodedFrames);
                _videoProvider.Login(new Common.Credentials() { IP = ip, Port = port, Username = username, Password = password });
                _cams = _videoProvider.Cameras();

                List<ListViewItem> items = new List<ListViewItem>();
                foreach (ICamera cam in _cams)
                {
                    int index = cam.IsOnline ? (cam.IsPtz ? ONLINE_PTZ : ONLINE_FIXED) : (cam.IsPtz ? OFFLINE_PTZ : OFFLINE_FIXED);
                    ListViewItem item = new ListViewItem(cam.FriendlyName, index);
                    item.Tag = cam;
                    items.Add(item);
                }

                // Ensure to make UI modifications on the UI thread
                this.BeginInvoke(new Action(() =>
                {
                    listViewCameras.Items.AddRange(items.ToArray());
                    panelVcr.Enabled = true;
                    buttonLogin.Enabled = true;
                }));
            }));
        }

        /// <summary>
        /// Event handler for when a mouse click has occurred on a panel (effectively selecting the panel)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_MouseUp(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel)sender;
            SelectPanel(panel);
        }

        /// <summary>
        /// Event handler for when the mouse has entered the panel's bounds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_MouseEnter(object sender, EventArgs e)
        {
            Panel display = (Panel)sender;
            Button exitButton = display.Controls.OfType<Button>().First();
            string id = SessionFromPanel(display);
            if (id != null)
                exitButton.Visible = true;
        }

        /// <summary>
        /// Event handler for when the mouse has left the top panel's bounds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel1_MouseLeave(object sender, EventArgs e)
        {
            Point point = splitContainer.Panel1.PointToClient(Control.MousePosition);
            if (!splitContainer.Panel1.ClientRectangle.Contains(point))
                buttonPanel1Exit.Visible = false;
        }

        /// <summary>
        /// Event handler for when the mouse has left the bottom panel's bounds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel2_MouseLeave(object sender, EventArgs e)
        {
            Point point = splitContainer.Panel2.PointToClient(Control.MousePosition);
            if (!splitContainer.Panel2.ClientRectangle.Contains(point))
                buttonPanel2Exit.Visible = false;
        }

        /// <summary>
        /// Event handler for when the the [x] has been clicked to disconnect the stream on the panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPanelExit_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Panel panel = (Panel)button.Parent;
            string id = SessionFromPanel(panel);
            if (!string.IsNullOrWhiteSpace(id))
            {
                _videoProvider.Stop(id);
                RemoveId(panel);
                panel.Refresh();
                button.Visible = false;
            }
        }

        /// <summary>
        /// Event handler for when the 1 hour ago playback button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonHourAgo_Click(object sender, EventArgs e)
        {
            _videoProvider.Seek(SelectedId, DateTime.UtcNow.AddHours(-1));
        }

        /// <summary>
        /// Event handler for when the live button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLive_Click(object sender, EventArgs e)
        {
            _videoProvider.GoLive(SelectedId);
        }

        /// <summary>
        /// Event handler for when the reverse button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonRev_Click(object sender, EventArgs e)
        {
            _videoProvider.Play(SelectedId, -4);
        }

        /// <summary>
        /// Event handler for when the frame reverse button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonFrameRev_Click(object sender, EventArgs e)
        {
            _videoProvider.FrameReverse(SelectedId);
        }

        /// <summary>
        /// Event handler for when the play button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPlay_Click(object sender, EventArgs e)
        {
            _videoProvider.Play(SelectedId, 1);
        }

        /// <summary>
        /// Event handler for when the pause button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPause_Click(object sender, EventArgs e)
        {
            _videoProvider.Pause(SelectedId);
        }

        /// <summary>
        /// Event handler for when the frame forward button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonFrameFw_Click(object sender, EventArgs e)
        {
            _videoProvider.FrameForward(SelectedId);
        }

        /// <summary>
        /// Event handler for when the fast forward button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonFw_Click(object sender, EventArgs e)
        {
            _videoProvider.Play(SelectedId, 4);
        }

        /// <summary>
        /// Method used to disconnect the providers and remove all cameras
        /// </summary>
        private void Disconnect()
        {
            panelVcr.Enabled = false;
            if (_videoProvider != null)
            {
                _videoProvider.Logout();
                _videoProvider.Dispose();
                _videoProvider = null;
            }
            foreach (ICamera cam in _cams)
                cam.Dispose();
            listViewCameras.Clear();
        }

        /// <summary>
        /// Get's the current ICamera associated with the specific list view item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private ICamera CameraFromLVItem(ListViewItem item)
        {
            return (ICamera)item.Tag;
        }

        /// <summary>
        /// Gets the current session id from the specific display
        /// </summary>
        /// <param name="display"></param>
        /// <returns></returns>
        private string SessionFromPanel(Panel display)
        {
            return (string)display.Tag;
        }

        /// <summary>
        /// Selects the specific panel as the selected panel to be used with playback controls
        /// </summary>
        /// <param name="panel"></param>
        private void SelectPanel(Panel panel)
        {
            if (_selectedPanel != null)
                _selectedPanel.GetChildAtPoint(new Point(0, 0)).BackColor = Color.Black;
            panel.GetChildAtPoint(new Point(0, 0)).BackColor = Color.ForestGreen;
            _selectedPanel = panel;
        }

        /// <summary>
        /// Removes the id from a panel
        /// </summary>
        /// <param name="panel"></param>
        private void RemoveId(Panel panel)
        {
            string id = SessionFromPanel(panel);
            if (id != null)
            {
                _ids.Remove(id);
                panel.Tag = null;
            }
        }

        /// <summary>
        /// Connects a stream to a display for viewing video
        /// </summary>
        /// <param name="item"></param>
        /// <param name="display"></param>
        /// <returns></returns>
        private string ConnectPlay(ListViewItem item, Panel display)
        {
            string existingId = SessionFromPanel(display);
            if (!string.IsNullOrWhiteSpace(existingId))
            {
                _videoProvider.Stop(existingId);
                RemoveId(display);
            }

            SelectPanel(display);
            ICamera camera = CameraFromLVItem(item);
            string id = _videoProvider.ConnectDisplay(display, camera);
            _videoProvider.Play(id, 1);
            display.Tag = id;
            _ids.Add(id);
            return id;
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            var rb = sender as RadioButton;
            if (rb.Checked)
            {
                flowLayoutPanelFrames.Enabled = rb == radioButtonObjectModel;
                if (!flowLayoutPanelFrames.Enabled)
                    checkBoxGetFrames.Checked = false;
            }
        }
    }
}
