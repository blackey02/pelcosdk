﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using PelcoAPI;

//namespace LiveVideo.SMWrapper
//{
//    class SMWrapperDeviceStorage : IDeviceStorageNet
//    {
//        private Dictionary<string, SMWrapperDeviceItem> _devices = new Dictionary<string, SMWrapperDeviceItem>();

//        /// <summary>
//        /// Returns a list of devices that are currently cached
//        /// </summary>
//        public List<SMWrapperDeviceItem> Devices
//        {
//            get
//            {
//                return _devices.Values.Select(item => new SMWrapperDeviceItem(item)).ToList();
//            }
//        }

//        #region IDeviceStorageNet Members
//        public void AddDevice(string sUDN, string sAttributes)
//        {
//            if (_devices.ContainsKey(sUDN))
//                _devices[sUDN].SetAttributes(sAttributes);
//            else
//                _devices.Add(sUDN, new SMWrapperDeviceItem(sUDN, sAttributes));
//        }

//        public void DeleteDevice(string sUDN)
//        {
//            if (_devices.ContainsKey(sUDN))
//                _devices.Remove(sUDN);
//        }

//        public void UpdateDevice(string sUDN, string sXmlAttributes)
//        {
//            if (_devices.ContainsKey(sUDN))
//                _devices[sUDN].SetAttributes(sXmlAttributes);
//            else
//                _devices.Add(sUDN, new SMWrapperDeviceItem(sUDN, sXmlAttributes));
//        }
//        #endregion
//    }
//}
