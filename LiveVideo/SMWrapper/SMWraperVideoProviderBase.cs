﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using PelcoAPI;
//using LiveVideo.Common;
//using System.Windows.Forms;

//namespace LiveVideo.SMWrapper
//{
//    abstract class SMWraperVideoProviderBase : VideoProviderBase
//    {
//        protected const string CAMERA = "Camera";
//        protected const string DDF_URL = "SYS_UpnpDevDescUrl";
//        protected const string VIDEO_OUTPUT = "VideoOutput";

//        protected abstract ICamera CreateCamera(SMWrapperDeviceItem item);

//        protected SystemManagerWrapperNet _smWrapper = new SystemManagerWrapperNet();
//        protected SMWrapperDeviceStorage _deviceStorage = new SMWrapperDeviceStorage();
//        protected int _loginId;
//        protected int _seqNum;

//        public SMWraperVideoProviderBase(bool isEdge, bool isRtsp)
//            : base(isEdge, isRtsp)
//        { }

//        protected override bool LoginImpl()
//        {
//            if (IsEdge)
//            {
//                _loginId = 1;
//            }
//            else
//            {
//                _smWrapper.SetSMAddress(_credentials.IP, _credentials.Port);
//                _loginId = _smWrapper.UserLogin(_credentials.Username, _credentials.Password);
//            }
//            return _loginId > 0;
//        }

//        protected override List<ICamera> CamerasImpl()
//        {
//            List<ICamera> cameras = new List<ICamera>();
//            if (IsEdge)
//            {
//                var device = new SMWrapperDeviceItem("edge");
//                var edgeInfo = string.Format("<name>SYS_DeviceState</name><value>2</value>");
//                edgeInfo += string.Format("<name>friendlyName</name><value>{0}</value>", _credentials.IP);
//                edgeInfo += string.Format("<name>SYS_UpnpDevDescUrl</name><value>http://{0}:{1}</value>", _credentials.IP, _credentials.Port);
//                device.SetAttributes(edgeInfo);
//                var edgeCam = CreateCamera(device);
//                cameras.Add(edgeCam);
//            }
//            else
//            {
//                bool success = _smWrapper.GetDevices(_loginId, ref _seqNum, CAMERA, _deviceStorage);
//                if (success)
//                {
//                    List<SMWrapperDeviceItem> devices = _deviceStorage.Devices;
//                    foreach (SMWrapperDeviceItem item in devices)
//                    {
//                        if (!string.IsNullOrWhiteSpace(item.NvrUuid))
//                        {
//                            string nvrDdf = string.Empty;
//                            string nvrServiceId = string.Empty;
//                            _smWrapper.GetDeviceAttributeValue(_loginId, item.NvrUuid, DDF_URL, ref nvrDdf);
//                            _smWrapper.GetServiceID(_loginId, item.NvrUuid, VIDEO_OUTPUT, ref nvrServiceId);
//                            item.NvrDdfUrl = nvrDdf;
//                            item.NvrVideoService = nvrServiceId;
//                        }
//                        string serviceId = string.Empty;
//                        _smWrapper.GetServiceID(_loginId, item.Uuid, VIDEO_OUTPUT, ref serviceId);
//                        item.VideoService = serviceId;
//                        ICamera camera = CreateCamera(item);
//                        cameras.Add(camera);
//                    }
//                }
//            }
//            return cameras;
//        }

//        protected override void LogoutImpl()
//        {
//            if (!IsEdge)
//                _smWrapper.UserLogout(_loginId);
//            _loginId = 0;
//        }

//        protected override void Dispose(bool disposed)
//        {
//            base.Dispose(disposed);
//            if (_smWrapper != null)
//            {
//                _smWrapper.Dispose();
//                _smWrapper = null;
//            }
//        }
//    }
//}
