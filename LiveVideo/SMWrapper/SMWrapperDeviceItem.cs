﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;

namespace LiveVideo.SMWrapper
{
    class SMWrapperDeviceItem
    {
        private const string FRIENDLY_NAME = "friendlyName";
        private const string UPNP_FRIENDLY_NAME = "SYS_UpnpFriendlyName";
        private const string CAMERA_TYPE = "CameraType";
        private const string DEVICE_STATE = "SYS_DeviceState";
        private const string DDF_URL = "SYS_UpnpDevDescUrl";
        private const string NVR_ASSOC = "SYS_NvrAssoc";
        private const string CAMERA_NUMBER = "CameraNumber";

        private string _uuid = string.Empty;
        private Dictionary<string, string> _attrib = new Dictionary<string, string>();

        public string VideoService { get; set; }
        public string NvrVideoService { get; set; }
        public string NvrDdfUrl { get; set; }

        public SMWrapperDeviceItem(string uuid, string attrib = null)
        {
            _uuid = uuid;
            VideoService = string.Empty;
            NvrVideoService = string.Empty;
            NvrDdfUrl = string.Empty;
            SetAttributes(attrib);
        }

        public SMWrapperDeviceItem(SMWrapperDeviceItem other) 
            : this(other._uuid)
        {
            _attrib = new Dictionary<string, string>(other._attrib);
        }

        public uint CameraNumber
        {
            get
            {
                uint number = 0;
                if (_attrib.ContainsKey(CAMERA_NUMBER))
                    number = uint.Parse(_attrib[CAMERA_NUMBER]);
                return number;
            }
        }

        /// <summary>
        /// Returns the camera's service id from the video output service string
        /// </summary>
        public int VideoServiceId
        {
            get
            {
                int id = 1;
                string pattern = @"(?<=urn:pelco-com:serviceId:VideoOutput-)\d+";
                Match match = Regex.Match(VideoService, pattern);
                if (match.Success)
                    id = int.Parse(match.Value);
                return id;
            }
        }

        /// <summary>
        /// Returns the associated nvr's service id from the video output service string
        /// </summary>
        public int NvrVideoServiceId
        {
            get
            {
                int id = 1;
                string pattern = @"(?<=urn:pelco-com:serviceId:VideoOutput-)\d+";
                Match match = Regex.Match(NvrVideoService, pattern);
                if (match.Success)
                    id = int.Parse(match.Value);
                return id;
            }
        }

        /// <summary>
        /// Returns the associated nvr's IP
        /// </summary>
        public string NvrIp
        {
            get
            {
                string ip = string.Empty;
                if (!string.IsNullOrWhiteSpace(NvrDdfUrl))
                    ip = new Uri(NvrDdfUrl).Host;
                return ip;
            }
        }

        /// <summary>
        /// Returns the associated nvr's port
        /// </summary>
        public ushort NvrPort
        {
            get
            {
                ushort port = 0;
                if (!string.IsNullOrWhiteSpace(NvrDdfUrl))
                    port = (ushort)new Uri(NvrDdfUrl).Port;
                return port;
            }
        }

        /// <summary>
        /// Returns the associated nvr's uuid
        /// </summary>
        public string NvrUuid
        {
            get
            {
                string nvrUuid = string.Empty;
                if (_attrib.ContainsKey(NVR_ASSOC))
                    nvrUuid = _attrib[NVR_ASSOC];
                return nvrUuid;
            }
        }

        /// <summary>
        /// Returns the camera's uuid
        /// </summary>
        public string Uuid
        {
            get
            {
                return _uuid;
            }
        }

        /// <summary>
        /// Returns the cameras IP
        /// </summary>
        public string Ip
        {
            get
            {
                string ip = string.Empty;
                if (_attrib.ContainsKey(DDF_URL))
                    ip = new Uri(_attrib[DDF_URL]).Host;
                return ip;
            }
        }

        /// <summary>
        /// Returns the camera's port
        /// </summary>
        public ushort Port
        {
            get
            {
                ushort port = 0;
                if (_attrib.ContainsKey(DDF_URL))
                    port = (ushort)new Uri(_attrib[DDF_URL]).Port;
                return port;
            }
        }

        /// <summary>
        /// Returns the camera's friendlyname
        /// </summary>
        public string FriendlyName
        {
            get
            {
                string friendlyName = string.Empty;
                if (_attrib.ContainsKey(FRIENDLY_NAME))
                    friendlyName = _attrib[FRIENDLY_NAME];
                else if (_attrib.ContainsKey(UPNP_FRIENDLY_NAME))
                    friendlyName = _attrib[UPNP_FRIENDLY_NAME];
                return friendlyName;
            }
        }

        /// <summary>
        /// Returns whether the camera is ptz or not
        /// </summary>
        public bool IsPtz
        {
            get
            {
                bool isPtz = false;
                if(_attrib.ContainsKey(CAMERA_TYPE))
                    isPtz = int.Parse(_attrib[CAMERA_TYPE]) > 0;
                return isPtz;
            }
        }

        /// <summary>
        /// Returns whether the camera is online or not
        /// </summary>
        public bool IsOnline
        {
            get
            {
                bool isOnline = false;
                if (_attrib.ContainsKey(DEVICE_STATE))
                    isOnline = int.Parse(_attrib[DEVICE_STATE]) == 2;
                return isOnline;
            }
        }

        /// <summary>
        /// Method used to set/update the attributes of this camera
        /// </summary>
        /// <param name="attrib"></param>
        public void SetAttributes(string attrib)
        {
            if(!string.IsNullOrWhiteSpace(attrib))
                ParseSmDevices(attrib);
        }

        /// <summary>
        /// Method used to parse the attributes of this camera in the form given by the smwrapper
        /// </summary>
        /// <param name="attrib"></param>
        private void ParseSmDevices(string attrib)
        {
            _attrib.Clear();
            attrib = string.Format("<root>{0}</root>", attrib);
            using (XmlTextReader reader = new XmlTextReader(new StringReader(attrib)))
            {
                ReadFlatXmlPairs(reader);
            }
        }

        /// <summary>
        /// Method used to read flat xml pairs
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="nameTag"></param>
        /// <param name="valueTag"></param>
        private void ReadFlatXmlPairs(XmlTextReader reader, string nameTag = "name", string valueTag = "value")
        {
            reader.MoveToContent();
            do
            {
                string key = string.Empty;
                string val = string.Empty;
                
                if (reader.LocalName == nameTag)
                {
                    key = reader.ReadString();
                    reader.Read();
                    if (reader.LocalName == valueTag)
                    {
                        val = reader.ReadString();
                        _attrib.Add(key, val);
                    }
                }
                else
                    reader.Read();
            } while (!reader.EOF);
        }
    }
}
