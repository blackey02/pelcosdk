﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiveVideo.Common;
using PelcoAPI;

namespace LiveVideo.ObjectModel
{
    /// <summary>
    /// ObjectCamera is a concrete implementation of ICamera, it's also used to manage all resources with the camera.
    /// </summary>
    class ObjectCamera : ICamera
    {
        //private ManagedPTZControlWrapperNet _ptzWrapper;
        public Pelco.SDK.Camera RawCamera { get; private set; }
        public Pelco.SDK.Stream RawStream { get; set; }
        public Pelco.SDK.Display RawDisplay { get; set; }
        private string _guid = Guid.NewGuid().ToString();

        public string ID { get { return _guid; } }

        public ObjectCamera(ObjectCamera obj)
            : this(new Pelco.SDK.Camera(obj.RawCamera))
        { }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="camera"></param>
        public ObjectCamera(Pelco.SDK.Camera camera)
        {
            RawCamera = camera;
            //if(IsOnline)
                //_ptzWrapper = new ManagedPTZControlWrapperNet(camera.IP, camera.Port, (int)camera.CameraNumber);
        }

        #region ICamera Members
        public string IP
        {
            get
            {
                return RawCamera.IP;
            }
        }

        public ushort Port
        {
            get
            {
                return (ushort)RawCamera.Port;
            }
        }

        public bool IsOnline 
        {
            get
            {
                return RawCamera.IsOnline;
            }
        }

        public bool IsPtz
        {
            get
            {
                bool isPtz = true;
                //isPtz = isPtz && _ptzWrapper != null;
                isPtz = isPtz && RawCamera.IsOnline;
                //isPtz = isPtz && _ptzWrapper.IsPTZCamera();
                return isPtz;
            }
        }

        public string FriendlyName
        {
            get
            {
                return RawCamera.FriendlyName;
            }
        }

        public int ServiceId
        {
            get
            {
                return (int)RawCamera.ChannelNumber;
            }
        }
        #endregion

        #region IDisposable Members
        protected virtual void Dispose(bool disposing)
        {
            //if (_ptzWrapper != null)
            {
                //_ptzWrapper.Dispose();
                //_ptzWrapper = null;
            }
            if (RawStream != null)
            {
                RawStream.Dispose();
                RawStream = null;
            }
            if (RawCamera != null)
            {
                RawCamera.Dispose();
                RawCamera = null;
            }
            if (RawDisplay != null)
            {
                RawDisplay.Dispose();
                RawDisplay = null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
