﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LiveVideo.ObjectModel
{
    class ObjectFrameInfo
    {
        public Control FrameDisplay { get; set; }
        public PictureBox FramePicBox { get; set; }
        public Pelco.SDK.Stream.MediaFrameDelegate EncodedDel { get; set; }
        public Pelco.SDK.Stream.MediaFrameDelegate DecodedDel { get; set; }
    }
}
