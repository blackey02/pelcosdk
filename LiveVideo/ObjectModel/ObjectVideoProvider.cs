﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiveVideo.Common;
using System.Drawing;
using System.Windows.Forms;

namespace LiveVideo.ObjectModel
{
    /// <summary>
    /// Provider using the Pelco SDK's object model to get and play cameras
    /// </summary>
    class ObjectVideoProvider : VideoProviderBase
    {
        Pelco.SDK.System _system;

        public ObjectVideoProvider(bool isEdge, bool isRtsp)
            : base(isEdge, isRtsp)
        { }

        protected override bool LoginImpl()
        {
            bool success = true;
            try
            {
                if (!IsEdge)
                {
                    foreach (var system in new Pelco.SDK.SystemCollection())
                    {
                        if (system.IP == _credentials.IP)
                        {
                            _system = system;
                            break;
                        }
                    }
                }

                if (_system == null)
                {
                    if (IsEdge)
                    {
                        var edgeCred = new Credentials(_credentials);
                        edgeCred.IP = string.Empty;
                        _system = new Pelco.SDK.System(CreateScheme(edgeCred));
                    }
                    else
                    {
                        _system = new Pelco.SDK.System(CreateScheme(_credentials));
                    }
                }
                try
                {
                    _system.Login(_credentials.Username, _credentials.Password);
                    success = true;
                }
                catch
                { }
            }
            catch (Pelco.SDK.Exception e)
            {
                System.Windows.Forms.MessageBox.Show(string.Format("Unable to connect to the system: {0}", e.Message));
                if (_system != null)
                {
                    _system.Dispose();
                    _system = null;
                }
                success = false;
            }
            return success;
        }

        protected override List<ICamera> CamerasImpl()
        {
            List<ICamera> cameras = new List<ICamera>();

            if (IsEdge)
            {
                var foundDevice = _system.DeviceCollection.FirstOrDefault(device => device.IP == _credentials.IP && device.Port == _credentials.Port);
                if (foundDevice == null)
                {
                    try
                    {
                        _system.DeviceCollection.Add(CreateScheme(_credentials));
                    }
                    catch (Exception)
                    { }
                }
            }

            IEnumerable<Pelco.SDK.Device> filteredCameras = _system.DeviceCollection.Where(device =>
                device.DeviceType == Pelco.SDK.DeviceType.Camera);
            foreach (Pelco.SDK.Device device in filteredCameras)
            {
                Pelco.SDK.Camera cameraDevice = new Pelco.SDK.Camera(device);
                ObjectCamera camera = new ObjectCamera(cameraDevice);
                cameras.Add(camera);
            }

            return cameras;
        }

        protected override void ConnectDisplayImpl(Control display, ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            objectCamera.RawStream = objectCamera.RawCamera.CreateStream();
            objectCamera.RawStream.StreamConfig = new Pelco.SDK.StreamConfiguration() 
            {
                DeliveryMode = Pelco.SDK.StreamDeliveryMode.Unicast,
                UnicastIp = Utils.Instance.LocalIpLike(objectCamera.RawCamera.IP),
                StreamProtocol = IsRtsp ? Pelco.SDK.StreamProtocol.Rtsp : Pelco.SDK.StreamProtocol.Rtp
            };
            objectCamera.RawDisplay = new Pelco.SDK.Display(display.Handle);
            objectCamera.RawDisplay.Show(objectCamera.RawStream);
        }

        protected override void SeekImpl(ICamera camera, DateTime date)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            try
            {
                objectCamera.RawStream.Seek(date);
                PlayImpl(camera, 1);
            }
            catch
            { }
        }

        protected override void PlayImpl(ICamera camera, float speed)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            try
            {
                objectCamera.RawStream.Play(speed);
            }
            catch (Pelco.SDK.Exception)
            { }
        }

        protected override void ResizeImpl(ICamera camera, Rectangle rect)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            objectCamera.RawDisplay.DisplayRect = rect;
        }

        protected override void FrameReverseImpl(ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            objectCamera.RawStream.FrameReverse();
        }

        protected override void FrameForwardImpl(ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            objectCamera.RawStream.FrameForward();
        }

        protected override void GoLiveImpl(ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            objectCamera.RawStream.GotoLive();
        }

        protected override void PauseImpl(ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            objectCamera.RawStream.Pause();
        }

        protected override void StopImpl(ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            objectCamera.RawStream.Stop();
            objectCamera.RawStream.Dispose();
            objectCamera.RawStream = null;

            if (objectCamera.RawDisplay != null)
            {
                objectCamera.RawDisplay.Dispose();
                objectCamera.RawDisplay = null;
            }
        }

        protected override void LogoutImpl()
        { }

        protected override void Dispose(bool disposed)
        {
            if (_system != null)
            {
                _system.Dispose();
                _system = null;
            }
            base.Dispose(disposed);
        }

        /// <summary>
        /// Creates the scheme necessary to login to the sm via the object model
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        private string CreateScheme(Credentials credentials)
        {
            string scheme = string.Empty;
            string socket = string.Empty;

            if (!string.IsNullOrWhiteSpace(credentials.IP))
                socket = credentials.IP;
            if (!string.IsNullOrWhiteSpace(socket) && credentials.Port != 0)
                socket = string.Format("{0}:{1}", socket, credentials.Port);

            scheme = string.Format("{0}:{1}@{2}://{3}", credentials.Username, credentials.Password,
                IsEdge ? "pelcoedgedevices" : "pelcosystem", (IsEdge && string.IsNullOrWhiteSpace(socket)) ? "?alias=LiveVideoSample" : socket);
            return scheme;
        }
    }
}