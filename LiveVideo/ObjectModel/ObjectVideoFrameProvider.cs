﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LiveVideo.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace LiveVideo.ObjectModel
{
    class ObjectVideoFrameProvider : ObjectVideoProvider
    {
        private Dictionary<string, ObjectFrameInfo> _frameInfo = new Dictionary<string, ObjectFrameInfo>();
        private bool _decodedFrames;

        public ObjectVideoFrameProvider(bool isEdge, bool isRtsp, bool decodedFrames)
            : base(isEdge, isRtsp)
        {
            _decodedFrames = decodedFrames;
        }

        protected override void ConnectDisplayImpl(Control display, ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;

            var frameInfo = new ObjectFrameInfo() { FrameDisplay = display, FramePicBox = CreatePictureBox() };
            frameInfo.EncodedDel = new Pelco.SDK.Stream.MediaFrameDelegate(frame => RawStream_EncodedMediaFrames(objectCamera, frame));
            frameInfo.DecodedDel = new Pelco.SDK.Stream.MediaFrameDelegate(frame => RawStream_DecodedMediaFrames(objectCamera, frame));

            objectCamera.RawStream = objectCamera.RawCamera.CreateStream();


            if (_decodedFrames)
                objectCamera.RawStream.DecodedMediaFrames += frameInfo.DecodedDel;
            else
                objectCamera.RawStream.EncodedMediaFrames += frameInfo.EncodedDel;

            objectCamera.RawStream.StreamConfig = new Pelco.SDK.StreamConfiguration()
            {
                DeliveryMode = Pelco.SDK.StreamDeliveryMode.Multicast,
                UnicastIp = Utils.Instance.LocalIpLike(objectCamera.RawCamera.IP),
                StreamProtocol = IsRtsp ? Pelco.SDK.StreamProtocol.Rtsp : Pelco.SDK.StreamProtocol.Rtp
            };

            _frameInfo.Add(objectCamera.ID, frameInfo);
        }

        private PictureBox CreatePictureBox()
        {
            var pb = new PictureBox();
            pb.Width = 300;
            pb.Height = 180;
            pb.Location = new Point(82, 0);
            pb.BackColor = Color.Black;
            pb.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            pb.SizeMode = PictureBoxSizeMode.Zoom;
            return pb;
        }

        private void RawStream_EncodedMediaFrames(ObjectCamera camera, Pelco.SDK.MediaFrame frame)
        {
            Debug.WriteLine(string.Format("{0} - {1}x{2}", frame.FrameTime, frame.Width, frame.Height));
        }

        private void RawStream_DecodedMediaFrames(ObjectCamera objectCamera, Pelco.SDK.MediaFrame frame)
        {
            DisplayImage(objectCamera, frame);
        }

        private void DisplayImage(ObjectCamera objectCamera, Pelco.SDK.MediaFrame frame)
        {
            if (_frameInfo[objectCamera.ID].FrameDisplay.InvokeRequired)
            {
                _frameInfo[objectCamera.ID].FrameDisplay.BeginInvoke(new Action(() => DisplayImage(objectCamera, frame)));
                return;
            }

            Bitmap bmp = CopyDataToBitmap(frame.Frame, (int)frame.Width, (int)frame.Height);
            if (bmp != null)
            {
                if (!_frameInfo[objectCamera.ID].FrameDisplay.Controls.Contains(_frameInfo[objectCamera.ID].FramePicBox))
                {
                    _frameInfo[objectCamera.ID].FrameDisplay.Controls.Add(_frameInfo[objectCamera.ID].FramePicBox);
                    _frameInfo[objectCamera.ID].FramePicBox.SendToBack();
                }
                _frameInfo[objectCamera.ID].FramePicBox.Image = bmp;
            }
        }

        private Bitmap CopyDataToBitmap(byte[] data, int width, int height)
        {
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            BitmapData bmpData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
            Marshal.Copy(data, 0, bmpData.Scan0, data.Length);
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        // Do nothing
        protected override void ResizeImpl(ICamera camera, Rectangle rect)
        { }

        protected override void StopImpl(ICamera camera)
        {
            ObjectCamera objectCamera = (ObjectCamera)camera;
            if (_decodedFrames)
                objectCamera.RawStream.DecodedMediaFrames -= _frameInfo[objectCamera.ID].DecodedDel;
            else
                objectCamera.RawStream.EncodedMediaFrames -= _frameInfo[objectCamera.ID].EncodedDel;

            // Wait for all events
            System.Threading.Thread.Sleep(500);

            _frameInfo[objectCamera.ID].FrameDisplay.Controls.Remove(_frameInfo[objectCamera.ID].FramePicBox);
            _frameInfo[objectCamera.ID].FramePicBox.Dispose();
            _frameInfo.Remove(objectCamera.ID);
            base.StopImpl(camera);
        }
    }
}
