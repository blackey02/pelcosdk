﻿namespace LiveVideo
{
    partial class LiveVideoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LiveVideoForm));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.buttonPanel1Exit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonPanel2Exit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxIp = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.listViewCameras = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buttonRev = new System.Windows.Forms.Button();
            this.buttonFrameRev = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.buttonFw = new System.Windows.Forms.Button();
            this.buttonFrameFw = new System.Windows.Forms.Button();
            this.buttonHourAgo = new System.Windows.Forms.Button();
            this.buttonLive = new System.Windows.Forms.Button();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.radioButtonObjectModel = new System.Windows.Forms.RadioButton();
            this.radioButtonApiViewer = new System.Windows.Forms.RadioButton();
            this.panelVcr = new System.Windows.Forms.Panel();
            this.checkBoxOnEdge = new System.Windows.Forms.CheckBox();
            this.radioButtonOcx = new System.Windows.Forms.RadioButton();
            this.checkBoxUseRtsp = new System.Windows.Forms.CheckBox();
            this.checkBoxGetFrames = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanelFrames = new System.Windows.Forms.FlowLayoutPanel();
            this.checkBoxDecoded = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panelVcr.SuspendLayout();
            this.flowLayoutPanelFrames.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(359, 34);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.AllowDrop = true;
            this.splitContainer.Panel1.Controls.Add(this.buttonPanel1Exit);
            this.splitContainer.Panel1.Controls.Add(this.panel1);
            this.splitContainer.Panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.splitContainer.Panel1.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            this.splitContainer.Panel1.MouseEnter += new System.EventHandler(this.Panel_MouseEnter);
            this.splitContainer.Panel1.MouseLeave += new System.EventHandler(this.Panel1_MouseLeave);
            this.splitContainer.Panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel_MouseUp);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.AllowDrop = true;
            this.splitContainer.Panel2.Controls.Add(this.buttonPanel2Exit);
            this.splitContainer.Panel2.Controls.Add(this.panel2);
            this.splitContainer.Panel2.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.splitContainer.Panel2.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            this.splitContainer.Panel2.MouseEnter += new System.EventHandler(this.Panel_MouseEnter);
            this.splitContainer.Panel2.MouseLeave += new System.EventHandler(this.Panel2_MouseLeave);
            this.splitContainer.Panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel_MouseUp);
            this.splitContainer.Size = new System.Drawing.Size(435, 419);
            this.splitContainer.SplitterDistance = 192;
            this.splitContainer.TabIndex = 0;
            // 
            // buttonPanel1Exit
            // 
            this.buttonPanel1Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPanel1Exit.AutoSize = true;
            this.buttonPanel1Exit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonPanel1Exit.BackColor = System.Drawing.Color.Transparent;
            this.buttonPanel1Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPanel1Exit.Location = new System.Drawing.Point(402, 3);
            this.buttonPanel1Exit.Name = "buttonPanel1Exit";
            this.buttonPanel1Exit.Padding = new System.Windows.Forms.Padding(2, 0, 0, 2);
            this.buttonPanel1Exit.Size = new System.Drawing.Size(26, 27);
            this.buttonPanel1Exit.TabIndex = 1;
            this.buttonPanel1Exit.Text = "x";
            this.buttonPanel1Exit.UseVisualStyleBackColor = false;
            this.buttonPanel1Exit.Visible = false;
            this.buttonPanel1Exit.Click += new System.EventHandler(this.ButtonPanelExit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(10, 10);
            this.panel1.TabIndex = 0;
            // 
            // buttonPanel2Exit
            // 
            this.buttonPanel2Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPanel2Exit.AutoSize = true;
            this.buttonPanel2Exit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonPanel2Exit.BackColor = System.Drawing.Color.Transparent;
            this.buttonPanel2Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPanel2Exit.Location = new System.Drawing.Point(402, 3);
            this.buttonPanel2Exit.Name = "buttonPanel2Exit";
            this.buttonPanel2Exit.Padding = new System.Windows.Forms.Padding(2, 0, 0, 2);
            this.buttonPanel2Exit.Size = new System.Drawing.Size(26, 27);
            this.buttonPanel2Exit.TabIndex = 2;
            this.buttonPanel2Exit.Text = "x";
            this.buttonPanel2Exit.UseVisualStyleBackColor = false;
            this.buttonPanel2Exit.Visible = false;
            this.buttonPanel2Exit.Click += new System.EventHandler(this.ButtonPanelExit_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 10);
            this.panel2.TabIndex = 1;
            // 
            // textBoxIp
            // 
            this.textBoxIp.Location = new System.Drawing.Point(7, 34);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new System.Drawing.Size(121, 20);
            this.textBoxIp.TabIndex = 3;
            this.textBoxIp.Text = "192.168.5.10";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(7, 189);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(121, 20);
            this.textBoxPassword.TabIndex = 4;
            this.textBoxPassword.Text = "admin";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(7, 137);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(121, 20);
            this.textBoxUsername.TabIndex = 5;
            this.textBoxUsername.Text = "admin";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(7, 85);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(121, 20);
            this.textBoxPort.TabIndex = 6;
            this.textBoxPort.Text = "60001";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "IP Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Port";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Username";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Password";
            // 
            // listViewCameras
            // 
            this.listViewCameras.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewCameras.LargeImageList = this.imageList1;
            this.listViewCameras.Location = new System.Drawing.Point(137, 34);
            this.listViewCameras.Name = "listViewCameras";
            this.listViewCameras.Size = new System.Drawing.Size(210, 448);
            this.listViewCameras.SmallImageList = this.imageList1;
            this.listViewCameras.TabIndex = 12;
            this.listViewCameras.UseCompatibleStateImageBehavior = false;
            this.listViewCameras.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListViewCameras_ItemDrag);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "camera_16x16.png");
            this.imageList1.Images.SetKeyName(1, "camera_offline_16x16.png");
            this.imageList1.Images.SetKeyName(2, "dome_16x16.png");
            this.imageList1.Images.SetKeyName(3, "dome_offline_16x16.png");
            // 
            // buttonRev
            // 
            this.buttonRev.Location = new System.Drawing.Point(84, 3);
            this.buttonRev.Name = "buttonRev";
            this.buttonRev.Size = new System.Drawing.Size(45, 23);
            this.buttonRev.TabIndex = 1;
            this.buttonRev.Text = "<<";
            this.buttonRev.UseVisualStyleBackColor = true;
            this.buttonRev.Click += new System.EventHandler(this.ButtonRev_Click);
            // 
            // buttonFrameRev
            // 
            this.buttonFrameRev.Location = new System.Drawing.Point(135, 3);
            this.buttonFrameRev.Name = "buttonFrameRev";
            this.buttonFrameRev.Size = new System.Drawing.Size(45, 23);
            this.buttonFrameRev.TabIndex = 2;
            this.buttonFrameRev.Text = "|<";
            this.buttonFrameRev.UseVisualStyleBackColor = true;
            this.buttonFrameRev.Click += new System.EventHandler(this.ButtonFrameRev_Click);
            // 
            // buttonPause
            // 
            this.buttonPause.Location = new System.Drawing.Point(237, 3);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(45, 23);
            this.buttonPause.TabIndex = 4;
            this.buttonPause.Text = "| |";
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Click += new System.EventHandler(this.ButtonPause_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Location = new System.Drawing.Point(186, 3);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(45, 23);
            this.buttonPlay.TabIndex = 5;
            this.buttonPlay.Text = ">";
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.ButtonPlay_Click);
            // 
            // buttonFw
            // 
            this.buttonFw.Location = new System.Drawing.Point(339, 3);
            this.buttonFw.Name = "buttonFw";
            this.buttonFw.Size = new System.Drawing.Size(45, 23);
            this.buttonFw.TabIndex = 6;
            this.buttonFw.Text = ">>";
            this.buttonFw.UseVisualStyleBackColor = true;
            this.buttonFw.Click += new System.EventHandler(this.ButtonFw_Click);
            // 
            // buttonFrameFw
            // 
            this.buttonFrameFw.Location = new System.Drawing.Point(288, 3);
            this.buttonFrameFw.Name = "buttonFrameFw";
            this.buttonFrameFw.Size = new System.Drawing.Size(45, 23);
            this.buttonFrameFw.TabIndex = 7;
            this.buttonFrameFw.Text = ">|";
            this.buttonFrameFw.UseVisualStyleBackColor = true;
            this.buttonFrameFw.Click += new System.EventHandler(this.ButtonFrameFw_Click);
            // 
            // buttonHourAgo
            // 
            this.buttonHourAgo.Location = new System.Drawing.Point(0, 3);
            this.buttonHourAgo.Name = "buttonHourAgo";
            this.buttonHourAgo.Size = new System.Drawing.Size(78, 23);
            this.buttonHourAgo.TabIndex = 8;
            this.buttonHourAgo.Text = "1 Hour Ago";
            this.buttonHourAgo.UseVisualStyleBackColor = true;
            this.buttonHourAgo.Click += new System.EventHandler(this.ButtonHourAgo_Click);
            // 
            // buttonLive
            // 
            this.buttonLive.Location = new System.Drawing.Point(390, 3);
            this.buttonLive.Name = "buttonLive";
            this.buttonLive.Size = new System.Drawing.Size(45, 23);
            this.buttonLive.TabIndex = 9;
            this.buttonLive.Text = "Live";
            this.buttonLive.UseVisualStyleBackColor = true;
            this.buttonLive.Click += new System.EventHandler(this.ButtonLive_Click);
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(7, 394);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(121, 23);
            this.buttonLogin.TabIndex = 13;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.ButtonLogin_Click);
            // 
            // radioButtonObjectModel
            // 
            this.radioButtonObjectModel.AutoSize = true;
            this.radioButtonObjectModel.Checked = true;
            this.radioButtonObjectModel.Location = new System.Drawing.Point(7, 225);
            this.radioButtonObjectModel.Name = "radioButtonObjectModel";
            this.radioButtonObjectModel.Size = new System.Drawing.Size(88, 17);
            this.radioButtonObjectModel.TabIndex = 14;
            this.radioButtonObjectModel.TabStop = true;
            this.radioButtonObjectModel.Text = "Object Model";
            this.radioButtonObjectModel.UseVisualStyleBackColor = true;
            this.radioButtonObjectModel.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // radioButtonApiViewer
            // 
            this.radioButtonApiViewer.AutoSize = true;
            this.radioButtonApiViewer.Location = new System.Drawing.Point(7, 302);
            this.radioButtonApiViewer.Name = "radioButtonApiViewer";
            this.radioButtonApiViewer.Size = new System.Drawing.Size(77, 17);
            this.radioButtonApiViewer.TabIndex = 15;
            this.radioButtonApiViewer.Text = "API Viewer";
            this.radioButtonApiViewer.UseVisualStyleBackColor = true;
            this.radioButtonApiViewer.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // panelVcr
            // 
            this.panelVcr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelVcr.AutoSize = true;
            this.panelVcr.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelVcr.Controls.Add(this.buttonHourAgo);
            this.panelVcr.Controls.Add(this.buttonRev);
            this.panelVcr.Controls.Add(this.buttonFrameRev);
            this.panelVcr.Controls.Add(this.buttonPause);
            this.panelVcr.Controls.Add(this.buttonLive);
            this.panelVcr.Controls.Add(this.buttonPlay);
            this.panelVcr.Controls.Add(this.buttonFw);
            this.panelVcr.Controls.Add(this.buttonFrameFw);
            this.panelVcr.Enabled = false;
            this.panelVcr.Location = new System.Drawing.Point(359, 459);
            this.panelVcr.Name = "panelVcr";
            this.panelVcr.Size = new System.Drawing.Size(438, 29);
            this.panelVcr.TabIndex = 2;
            // 
            // checkBoxOnEdge
            // 
            this.checkBoxOnEdge.AutoSize = true;
            this.checkBoxOnEdge.Location = new System.Drawing.Point(7, 348);
            this.checkBoxOnEdge.Name = "checkBoxOnEdge";
            this.checkBoxOnEdge.Size = new System.Drawing.Size(51, 17);
            this.checkBoxOnEdge.TabIndex = 16;
            this.checkBoxOnEdge.Text = "Edge";
            this.checkBoxOnEdge.UseVisualStyleBackColor = true;
            // 
            // radioButtonOcx
            // 
            this.radioButtonOcx.AutoSize = true;
            this.radioButtonOcx.Location = new System.Drawing.Point(7, 325);
            this.radioButtonOcx.Name = "radioButtonOcx";
            this.radioButtonOcx.Size = new System.Drawing.Size(82, 17);
            this.radioButtonOcx.TabIndex = 17;
            this.radioButtonOcx.Text = "OCX Viewer";
            this.radioButtonOcx.UseVisualStyleBackColor = true;
            this.radioButtonOcx.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // checkBoxUseRtsp
            // 
            this.checkBoxUseRtsp.AutoSize = true;
            this.checkBoxUseRtsp.Location = new System.Drawing.Point(7, 371);
            this.checkBoxUseRtsp.Name = "checkBoxUseRtsp";
            this.checkBoxUseRtsp.Size = new System.Drawing.Size(55, 17);
            this.checkBoxUseRtsp.TabIndex = 18;
            this.checkBoxUseRtsp.Text = "RTSP";
            this.checkBoxUseRtsp.UseVisualStyleBackColor = true;
            // 
            // checkBoxGetFrames
            // 
            this.checkBoxGetFrames.AutoSize = true;
            this.checkBoxGetFrames.Location = new System.Drawing.Point(3, 3);
            this.checkBoxGetFrames.Name = "checkBoxGetFrames";
            this.checkBoxGetFrames.Size = new System.Drawing.Size(85, 17);
            this.checkBoxGetFrames.TabIndex = 19;
            this.checkBoxGetFrames.Text = "Raw Frames";
            this.checkBoxGetFrames.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanelFrames
            // 
            this.flowLayoutPanelFrames.Controls.Add(this.checkBoxGetFrames);
            this.flowLayoutPanelFrames.Controls.Add(this.checkBoxDecoded);
            this.flowLayoutPanelFrames.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelFrames.Location = new System.Drawing.Point(24, 248);
            this.flowLayoutPanelFrames.Name = "flowLayoutPanelFrames";
            this.flowLayoutPanelFrames.Size = new System.Drawing.Size(104, 48);
            this.flowLayoutPanelFrames.TabIndex = 3;
            this.flowLayoutPanelFrames.WrapContents = false;
            // 
            // checkBoxDecoded
            // 
            this.checkBoxDecoded.AutoSize = true;
            this.checkBoxDecoded.Location = new System.Drawing.Point(3, 26);
            this.checkBoxDecoded.Name = "checkBoxDecoded";
            this.checkBoxDecoded.Size = new System.Drawing.Size(70, 17);
            this.checkBoxDecoded.TabIndex = 20;
            this.checkBoxDecoded.Text = "Decoded";
            this.checkBoxDecoded.UseVisualStyleBackColor = true;
            // 
            // LiveVideoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 496);
            this.Controls.Add(this.flowLayoutPanelFrames);
            this.Controls.Add(this.checkBoxUseRtsp);
            this.Controls.Add(this.radioButtonOcx);
            this.Controls.Add(this.checkBoxOnEdge);
            this.Controls.Add(this.panelVcr);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.radioButtonApiViewer);
            this.Controls.Add(this.radioButtonObjectModel);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.listViewCameras);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.textBoxUsername);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxIp);
            this.MinimumSize = new System.Drawing.Size(820, 534);
            this.Name = "LiveVideoForm";
            this.Text = "Live Video Sample";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LiveVideoForm_FormClosing);
            this.Resize += new System.EventHandler(this.LiveVideoForm_Resize);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panelVcr.ResumeLayout(false);
            this.flowLayoutPanelFrames.ResumeLayout(false);
            this.flowLayoutPanelFrames.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxIp;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView listViewCameras;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button buttonFrameFw;
        private System.Windows.Forms.Button buttonFw;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonFrameRev;
        private System.Windows.Forms.Button buttonRev;
        private System.Windows.Forms.Button buttonLive;
        private System.Windows.Forms.Button buttonHourAgo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.RadioButton radioButtonObjectModel;
        private System.Windows.Forms.RadioButton radioButtonApiViewer;
        private System.Windows.Forms.Panel panelVcr;
        private System.Windows.Forms.Button buttonPanel1Exit;
        private System.Windows.Forms.Button buttonPanel2Exit;
        private System.Windows.Forms.CheckBox checkBoxOnEdge;
        private System.Windows.Forms.RadioButton radioButtonOcx;
        private System.Windows.Forms.CheckBox checkBoxUseRtsp;
        private System.Windows.Forms.CheckBox checkBoxGetFrames;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFrames;
        private System.Windows.Forms.CheckBox checkBoxDecoded;
    }
}

