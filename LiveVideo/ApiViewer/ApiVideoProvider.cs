﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using LiveVideo.Common;
//using System.Drawing;
//using PelcoAPI;
//using System.Windows.Forms;
//using LiveVideo.SMWrapper;

//namespace LiveVideo.ApiViewer
//{
//    /// <summary>
//    /// Provider using smwrapper and the API viewer to get and play cameras
//    /// </summary>
//    class ApiVideoProvider : SMWraperVideoProviderBase
//    {
//        public ApiVideoProvider(bool isEdge, bool isRtsp)
//            : base(isEdge, isRtsp)
//        { }

//        protected override ICamera CreateCamera(SMWrapperDeviceItem item)
//        {
//            return new ApiCamera(item);
//        }

//        protected override void ConnectDisplayImpl(Control display, ICamera camera)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            CreateStream(apiCamera, display);
//        }

//        protected override void SeekImpl(ICamera camera, DateTime date)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            CreateStream(apiCamera, null, date, DateTime.UtcNow);
//            PlayImpl(apiCamera, 1);
//        }

//        protected override void PlayImpl(ICamera camera, float speed)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            apiCamera.ApiViewer.PlayForward(apiCamera.SessionId, speed);
//        }

//        protected override void FrameReverseImpl(ICamera camera)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            apiCamera.ApiViewer.FrameReverse(apiCamera.SessionId);
//        }

//        protected override void FrameForwardImpl(ICamera camera)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            apiCamera.ApiViewer.FrameForward(apiCamera.SessionId);
//        }

//        protected override void GoLiveImpl(ICamera camera)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            CreateStream(apiCamera);
//            PlayImpl(apiCamera, 1);
//        }

//        protected override void ResizeImpl(ICamera camera, Rectangle rect)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            apiCamera.ApiViewer.SetDisplayRect(rect.Top, rect.Left, rect.Width, rect.Height);
//        }

//        protected override void PauseImpl(ICamera camera)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            apiCamera.ApiViewer.Pause(apiCamera.SessionId);
//        }

//        protected override void StopImpl(ICamera camera)
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;
//            apiCamera.ApiViewer.StopStream(apiCamera.SessionId);
//            apiCamera.ApiViewer.Dispose();
//            apiCamera.ApiViewer = null;
//            apiCamera.SessionId = null;
//        }

//        /// <summary>
//        /// Creates a stream depending on the parameters given.
//        /// The stream could be a live or playback stream.
//        /// This method also auto selects a rtp port to be used when streaming video.
//        /// </summary>
//        /// <param name="camera"></param>
//        /// <param name="display"></param>
//        /// <param name="startTime"></param>
//        /// <param name="endTime"></param>
//        private void CreateStream(ICamera camera, Control display = null, DateTime startTime = default(DateTime), DateTime endTime = default(DateTime))
//        {
//            ApiCamera apiCamera = (ApiCamera)camera;

//            if(display != null)
//                apiCamera.Display = display;

//            if (!string.IsNullOrWhiteSpace(apiCamera.SessionId))
//                StopImpl(apiCamera);

//            string strVideoServiceId = apiCamera.DeviceItem.VideoServiceId.ToString();
//            string strIp = apiCamera.DeviceItem.Ip;
//            string strPort = apiCamera.DeviceItem.Port.ToString();
//            string strStartTime = startTime == default(DateTime) ? NOW : startTime.ToString("s");
//            string strEndTime = endTime == default(DateTime) ? INFINITE : endTime.ToString("s");
//            string strRtspUrl = GetRtspStream(apiCamera);

//            if (strStartTime != NOW)
//            {
//                strVideoServiceId = apiCamera.DeviceItem.NvrVideoServiceId.ToString();
//                strIp = apiCamera.DeviceItem.NvrIp;
//                strPort = apiCamera.DeviceItem.NvrPort.ToString();
//                bool containsQMark = strRtspUrl.Contains("?");
//                string firstDelimiter = containsQMark ? "&" : "?";
//                strRtspUrl = string.Format("{0}{1}starttime={2}&endtime={3}", strRtspUrl, firstDelimiter, strStartTime, strEndTime);
//            }

//            apiCamera.ApiViewer = new PelcoAPIViewerNet();
//            apiCamera.ApiViewer.SetPluginDir(Utils.Instance.PluginDir);
//            apiCamera.ApiViewer.SetWindowHandle(apiCamera.Display.Handle);

//            if (apiCamera.ApiViewer.IsAuthenticationEnabled(strIp, strPort, strVideoServiceId))
//            {
//                AuthenticationCredentialsNet auth = new AuthenticationCredentialsNet(_credentials.Username, _credentials.Password, AuthenticationSchemeTypeNet.Basic);
//                apiCamera.ApiViewer.SetAuthenticationCredentials(auth);
//            }

//            var myIp = Utils.Instance.LocalIpLike(strIp);

//            for (int i = 0; i < 2; i++)
//            {
//                string transport = string.Format("rtp://{0}:{1}", myIp, TRANS_PORT);
//                // We're incrementing by 4 because the RTP must always be even and it's possible to have
//                // an associated audio stream which would take up the selected rtp port + 2
//                TRANS_PORT += 4;

//                if (IsRtsp)
//                {
//                    apiCamera.SessionId = apiCamera.ApiViewer.StartStream(strRtspUrl, _credentials.Username,
//                        _credentials.Password, false, null);
//                }
//                else
//                {
//                    apiCamera.SessionId = apiCamera.ApiViewer.StartStream(strStartTime, strEndTime, strIp, strPort,
//                        strVideoServiceId, transport, apiCamera.DeviceItem.Uuid,
//                        apiCamera.DeviceItem.NvrVideoServiceId.ToString(), null, true, false, null);    
//                }
                
//                if (!string.IsNullOrWhiteSpace(apiCamera.SessionId))
//                    break;
//            }
//        }
//    }
//}