﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using LiveVideo.Common;
//using PelcoAPI;
//using System.Windows.Forms;
//using LiveVideo.SMWrapper;

//namespace LiveVideo.ApiViewer
//{
//    /// <summary>
//    /// ApiCamera is a concrete implementation of ICamera, it's also used to manage all resources with the camera.
//    /// </summary>
//    class ApiCamera : ICamera
//    {
//        public SMWrapperDeviceItem DeviceItem { get; private set; }
//        public PelcoAPIViewerNet ApiViewer { get; set; }
//        public Control Display { get; set; }
//        public string SessionId { get; set; }
//        private string _guid = Guid.NewGuid().ToString();

//        public string ID { get { return _guid; } }

//        /// <summary>
//        /// Ctor
//        /// </summary>
//        /// <param name="deviceItem"></param>
//        public ApiCamera(SMWrapperDeviceItem deviceItem)
//        {
//            DeviceItem = deviceItem;
//        }

//        public ApiCamera(ApiCamera obj)
//            : this(obj.DeviceItem)
//        {}

//        #region ICamera Members
//        public string IP
//        {
//            get
//            {
//                return DeviceItem.Ip;
//            }
//        }
        
//        public ushort Port
//        {
//            get
//            {
//                return DeviceItem.Port;
//            }
//        }

//        public bool IsPtz
//        {
//            get 
//            {
//                return DeviceItem.IsPtz;
//            }
//        }

//        public bool IsOnline
//        {
//            get 
//            {
//                return DeviceItem.IsOnline;
//            }
//        }

//        public string FriendlyName
//        {
//            get 
//            {
//                return DeviceItem.FriendlyName;
//            }
//        }

//        public int ServiceId
//        {
//            get 
//            { 
//                return DeviceItem.VideoServiceId; 
//            }
//        }
//        #endregion

//        #region IDisposable Members
//        protected void Dispose(bool disposed)
//        {
//            if (ApiViewer != null)
//            {
//                ApiViewer.Dispose();
//                ApiViewer = null;
//            }
//        }

//        public void Dispose()
//        {
//            Dispose(true);
//            GC.SuppressFinalize(this);
//        }
//        #endregion
//    }
//}
