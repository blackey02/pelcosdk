﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using LiveVideo.Common;
//using AxPelcoAPICOMViewerLib;
//using System.Windows.Forms;
//using LiveVideo.SMWrapper;

//namespace LiveVideo.Ocx
//{
//    class OcxCamera : ICamera
//    {
//        public SMWrapperDeviceItem DeviceItem { get; private set; }
//        public AxPelcoAPICOMViewer ComViewer { get; set; }
//        public Control Display { get; set; }
//        public string SessionId { get; set; }
//        private string _guid = Guid.NewGuid().ToString();

//        public string ID { get { return _guid; } }

//        public OcxCamera(SMWrapperDeviceItem deviceItem)
//        {
//            DeviceItem = deviceItem;
//        }

//        public OcxCamera(OcxCamera obj)
//            : this(obj.DeviceItem)
//        { }

//        #region ICamera Members
//        public string IP
//        {
//            get
//            {
//                return DeviceItem.Ip;
//            }
//        }

//        public ushort Port
//        {
//            get
//            {
//                return DeviceItem.Port;
//            }
//        }

//        public bool IsPtz
//        {
//            get
//            {
//                return DeviceItem.IsPtz;
//            }
//        }

//        public bool IsOnline
//        {
//            get
//            {
//                return DeviceItem.IsOnline;
//            }
//        }

//        public string FriendlyName
//        {
//            get
//            {
//                return DeviceItem.FriendlyName;
//            }
//        }

//        public int ServiceId
//        {
//            get
//            {
//                return DeviceItem.VideoServiceId;
//            }
//        }
//        #endregion

//        #region IDisposable Members
//        public void Dispose()
//        {
//            Dispose(true);
//            GC.SuppressFinalize(this);
//        }

//        protected void Dispose(bool disposed)
//        {
//            if (ComViewer != null)
//            {
//                ComViewer.Dispose();
//                ComViewer = null;
//            }
//        }
//        #endregion
//    }
//}
