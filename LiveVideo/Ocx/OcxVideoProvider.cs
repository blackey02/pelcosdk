﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using LiveVideo.Common;
//using AxPelcoAPICOMViewerLib;
//using System.Windows.Forms;
//using LiveVideo.SMWrapper;
//using PelcoAPI;

//namespace LiveVideo.Ocx
//{
//    class OcxVideoProvider : SMWraperVideoProviderBase
//    {
//        public OcxVideoProvider(bool isEdge, bool isRtsp)
//            : base(isEdge, isRtsp)
//        { }

//        protected override ICamera CreateCamera(SMWrapperDeviceItem item)
//        {
//            return new OcxCamera(item);
//        }

//        protected override void ConnectDisplayImpl(Control display, ICamera camera)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            CreateStream(ocxCamera, display);
//        }

//        protected override void SeekImpl(ICamera camera, DateTime date)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            CreateStream(ocxCamera, null, date, DateTime.UtcNow);
//            PlayImpl(ocxCamera, 1);
//        }

//        protected override void PlayImpl(ICamera camera, float speed)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            ocxCamera.ComViewer.PlayForward(ocxCamera.SessionId, 1.0f);
//        }

//        protected override void ResizeImpl(ICamera camera, System.Drawing.Rectangle rect)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            ocxCamera.ComViewer.SetBounds(rect.X, rect.Y, rect.Width, rect.Height);
//        }

//        protected override void FrameReverseImpl(ICamera camera)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            ocxCamera.ComViewer.FrameReverse(ocxCamera.SessionId);
//        }

//        protected override void FrameForwardImpl(ICamera camera)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            ocxCamera.ComViewer.FrameForward(ocxCamera.SessionId);
//        }

//        protected override void GoLiveImpl(ICamera camera)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            CreateStream(ocxCamera);
//            PlayImpl(ocxCamera, 1);
//        }

//        protected override void PauseImpl(ICamera camera)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            ocxCamera.ComViewer.Pause(ocxCamera.SessionId);
//        }

//        protected override void StopImpl(ICamera camera)
//        {
//            OcxCamera ocxCamera = (OcxCamera)camera;
//            ocxCamera.ComViewer.StopStream(ocxCamera.SessionId);
//            ocxCamera.ComViewer.Dispose();
//            ocxCamera.ComViewer = null;
//            ocxCamera.SessionId = null;
//        }

//        private void CreateStream(ICamera camera, Control display = null, DateTime startTime = default(DateTime), DateTime endTime = default(DateTime))
//        {
//            var ocxCamera = (OcxCamera)camera;

//            if (display != null)
//                ocxCamera.Display = display;

//            if (!string.IsNullOrWhiteSpace(ocxCamera.SessionId))
//                StopImpl(ocxCamera);

//            string strVideoServiceId = ocxCamera.DeviceItem.VideoServiceId.ToString();
//            string strIp = ocxCamera.DeviceItem.Ip;
//            string strPort = ocxCamera.DeviceItem.Port.ToString();
//            string strStartTime = startTime == default(DateTime) ? NOW : startTime.ToString("s");
//            string strEndTime = endTime == default(DateTime) ? INFINITE : endTime.ToString("s");
//            string strRtspUrl = GetRtspStream(ocxCamera);

//            if (strStartTime != NOW)
//            {
//                strVideoServiceId = ocxCamera.DeviceItem.NvrVideoServiceId.ToString();
//                strIp = ocxCamera.DeviceItem.NvrIp;
//                strPort = ocxCamera.DeviceItem.NvrPort.ToString();
//                bool containsQMark = strRtspUrl.Contains("?");
//                string firstDelimiter = containsQMark ? "&" : "?";
//                strRtspUrl = string.Format("{0}{1}starttime={2}&endtime={3}", strRtspUrl, firstDelimiter, strStartTime, strEndTime);
//            }

//            ocxCamera.ComViewer = new AxPelcoAPICOMViewer();
//            ocxCamera.Display.Controls.Add(ocxCamera.ComViewer);
//            ocxCamera.ComViewer.Hide();
//            ocxCamera.ComViewer.SetPluginDir(Utils.Instance.PluginDir);
//            ocxCamera.ComViewer.SetWindowHandle(ocxCamera.Display.Handle.ToInt32());

//            if (ocxCamera.ComViewer.IsAuthenticationEnabled(_credentials.IP, _credentials.Port.ToString(), "1"))
//                ocxCamera.ComViewer.SetAuthentication(_credentials.Username, _credentials.Password, "Basic");

//            var myIp = Utils.Instance.LocalIpLike(strIp);

//            for (int i = 0; i < 2; i++)
//            {
//                string transport = string.Format("rtp://{0}:{1}", myIp, TRANS_PORT);
//                // We're incrementing by 4 because the RTP must always be even and it's possible to have
//                // an associated audio stream which would take up the selected rtp port + 2
//                TRANS_PORT += 4;

//                if (IsRtsp)
//                {
//                    ocxCamera.SessionId = ocxCamera.ComViewer.StartRtspStream(strRtspUrl, _credentials.Username,
//                        _credentials.Password, false);
//                }
//                else
//                {
//                    ocxCamera.SessionId = ocxCamera.ComViewer.StartStreamEx(strStartTime, strEndTime, strIp, strPort,
//                        strVideoServiceId, transport, ocxCamera.DeviceItem.Uuid, ocxCamera.DeviceItem.NvrVideoServiceId.ToString(),
//                        true, false);
//                }
                
//                if (!string.IsNullOrWhiteSpace(ocxCamera.SessionId))
//                    break;
//            }
//        }
//    }
//}
