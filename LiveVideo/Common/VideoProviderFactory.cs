﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiveVideo.ObjectModel;
//using LiveVideo.ApiViewer;
//using LiveVideo.Ocx;

namespace LiveVideo.Common
{
    /// <summary>
    /// The different types of providers supported
    /// </summary>
    public enum VideoProviderType
    {
        Unknown,
        ObjectVideo,
        ApiVideo,
        OcxVideo,
        ObjectVideoFrames
    }

    /// <summary>
    ///  Factory used to create video providers as to keep the code loosely coupled
    /// </summary>
    class VideoProviderFactory
    {
        public static IVideoProvider CreateVideoProvider(VideoProviderType type, bool isEdge, bool isRtsp, 
            bool decodedFrames)
        {
            IVideoProvider provider = null;
            if (type == VideoProviderType.ObjectVideo)
                provider = new ObjectVideoProvider(isEdge, isRtsp);
            //else if (type == VideoProviderType.ApiVideo)
                //provider = new ApiVideoProvider(isEdge, isRtsp);
            //else if (type == VideoProviderType.OcxVideo)
                //provider = new OcxVideoProvider(isEdge, isRtsp);
            else if (type == VideoProviderType.ObjectVideoFrames)
                provider = new ObjectVideoFrameProvider(isEdge, isRtsp, decodedFrames);
            return provider;
        }
    }
}
