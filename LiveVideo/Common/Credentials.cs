﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LiveVideo.Common
{
    /// <summary>
    /// Credentials are used to encapsulate the information needed to login to a SM
    /// </summary>
    class Credentials
    {
        public string IP { get; set; }
        public ushort Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public Credentials()
        { }

        public Credentials(Credentials cred)
        {
            IP = cred.IP;
            Port = cred.Port;
            Username = cred.Username;
            Password = cred.Password;
        }
    }
}
