﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace LiveVideo.Common
{
    /// <summary>
    /// Common utilities or helper functions
    /// </summary>
    partial class Utils
    {
        private static Utils instance = null;

#if (DEBUG)
        private const string CONFIG_NAME = "Debug";
#else
        private const string CONFIG_NAME = "Release";
#endif

        /// <summary>
        /// Main endpoint into this singleton
        /// </summary>
        public static Utils Instance
        {
            get
            {
                if (Utils.instance == null)
                    Utils.instance = new Utils();
                return Utils.instance;
            }
        }

        /// <summary>
        /// Private ctor as not to be used by the public
        /// </summary>
        private Utils()
        { }

        /// <summary>
        /// The plugin directory.
        /// Required specifically when using the api viewer.
        /// </summary>
        public string PluginDir
        {
            get
            {
                string pluginsDir = Path.Combine(System.Windows.Forms.Application.StartupPath, "Plugins");
                string sdkDir = Environment.GetEnvironmentVariable("PELCO_SDK");
                if(!string.IsNullOrWhiteSpace(sdkDir))
                    pluginsDir = Path.Combine(sdkDir, "Libs", CONFIG_NAME, "Plugins");
                return pluginsDir;
            }
        }

        /// <summary>
        /// Returns a list of ipv4 addresses currently assigned to the system.
        /// </summary>
        public List<string> IPv4
        {
            get
            {
                List<string> ips = new List<string>();
                IPAddress[] ipv4Addresses = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, 
                    a => a.AddressFamily == AddressFamily.InterNetwork && !a.ToString().StartsWith("169."));
                ips = ipv4Addresses.Select(ip => ip.ToString()).ToList();
                return ips;
            }
        }

        public string LocalIpLike(string likeIp)
        {
            return IPv4.FirstOrDefault(ip => ip.Trim().StartsWith(likeIp.Trim().Substring(0, 3)));
        }
    }
}
