﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LiveVideo.Common
{
    /// <summary>
    /// Represents a video provider for getting cameras and playing video
    /// </summary>
    interface IVideoProvider : IDisposable
    {
        bool IsEdge { get; }
        bool IsRtsp { get; }
        bool Login(Credentials credentials);
        List<ICamera> Cameras();
        string ConnectDisplay(Control display, ICamera camera);
        void Seek(string id, DateTime date);
        void Play(string id, float speed);
        void FrameReverse(string id);
        void FrameForward(string id);
        void GoLive(string id);
        void Pause(string id);
        void Stop(string id);
        void Resize();
        void Logout();
    }
}
