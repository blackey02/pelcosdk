﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LiveVideo.Common
{
    /// <summary>
    /// Represents a Pelco camera device
    /// </summary>
    interface ICamera : IDisposable
    {
        string ID { get; }
        string IP { get; }
        ushort Port { get; }
        bool IsPtz { get; }
        bool IsOnline { get; }
        string FriendlyName { get; }
        int ServiceId { get; }
    }
}
