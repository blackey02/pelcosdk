﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace LiveVideo.Common
{
    /// <summary>
    /// Abstract base class for the video providers, which takes care of a lot of repetitive or common tasks.
    /// </summary>
    abstract class VideoProviderBase : IVideoProvider
    {
        protected static ushort TRANS_PORT = 9002;
        protected const string NOW = "NOW";
        protected const string INFINITE = "INFINITE";

        // Protected/Private
        protected abstract bool LoginImpl();
        protected Credentials _credentials;
        private bool _isEdge;
        private bool _isRtsp;

        // These methods must be implemented in the concrete derived classes
        protected abstract List<ICamera> CamerasImpl();
        protected abstract void ConnectDisplayImpl(Control display, ICamera camera);
        protected abstract void SeekImpl(ICamera camera, DateTime date);
        protected abstract void PlayImpl(ICamera camera, float speed);
        protected abstract void ResizeImpl(ICamera camera, Rectangle rect);
        protected abstract void FrameReverseImpl(ICamera camera);
        protected abstract void FrameForwardImpl(ICamera camera);
        protected abstract void GoLiveImpl(ICamera camera);
        protected abstract void PauseImpl(ICamera camera);
        protected abstract void StopImpl(ICamera camera);
        protected abstract void LogoutImpl();

        static VideoProviderBase()
        {
            System.Net.ServicePointManager.Expect100Continue = false;
        }

        public VideoProviderBase(bool isEdge, bool isRtsp)
        {
            IsEdge = isEdge;
            IsRtsp = isRtsp;
        }

        public bool IsRtsp
        {
            get { return _isRtsp; }
            protected set { _isRtsp = value; }
        }

        public bool IsEdge
        {
            get { return _isEdge; }
            protected set { _isEdge = value; }
        }

        /// <summary>
        /// Class used to manage which camera is associated with which display
        /// </summary>
        private class CameraManagerItem
        {
            public ICamera Camera { get; set; }
            public Control Display { get; set; }
        }

        private Dictionary<string, CameraManagerItem> _cameraManager = new Dictionary<string, CameraManagerItem>();
        protected bool _isLoggedIn = false;

        #region IVideoProvider Members
        public bool Login(Credentials credentials)
        {
            if (_isLoggedIn)
                return true;
            _credentials = credentials;
            _isLoggedIn = LoginImpl();
            return _isLoggedIn;
        }

        public List<ICamera> Cameras()
        {
            List<ICamera> cameras = new List<ICamera>();
            if (!_isLoggedIn)
                return cameras;
            cameras = CamerasImpl();
            return cameras;
        }

        public string ConnectDisplay(Control display, ICamera camera)
        {
            if (!_isLoggedIn)
                return string.Empty;

            string id = FindId(display);
            if (!string.IsNullOrWhiteSpace(id))
                Stop(id);

            string _uuid = Guid.NewGuid().ToString();
            CameraManagerItem manager = new CameraManagerItem();
            manager.Camera = (ICamera)Activator.CreateInstance(camera.GetType(), camera);
            manager.Display = display;
            ConnectDisplayImpl(manager.Display, manager.Camera);
            _cameraManager.Add(_uuid, manager);
            return _uuid;
        }

        public void Seek(string id, DateTime date)
        {
            if (_isEdge) return;

            CameraManagerItem manager = FindCameraManager(id);
            if (!_isLoggedIn || manager == null) return;
            SeekImpl(manager.Camera, date);
        }

        public void Play(string id, float speed)
        {
            if (_isEdge && (speed < 1.0 || speed > 1.0)) return;

            CameraManagerItem manager = FindCameraManager(id);
            if (!_isLoggedIn || manager == null) return;
            PlayImpl(manager.Camera, speed);
        }

        public void FrameReverse(string id)
        {
            if (_isEdge) return;

            CameraManagerItem manager = FindCameraManager(id);
            if (!_isLoggedIn || manager == null) return;
            FrameReverseImpl(manager.Camera);
        }

        public void FrameForward(string id)
        {
            if (_isEdge) return;

            CameraManagerItem manager = FindCameraManager(id);
            if (!_isLoggedIn || manager == null) return;
            FrameForwardImpl(manager.Camera);
        }

        public void Resize()
        {
            foreach (CameraManagerItem item in _cameraManager.Values)
                ResizeImpl(item.Camera, ((Control)item.Display).DisplayRectangle);
        }

        public void GoLive(string id)
        {
            if (_isEdge) return;

            CameraManagerItem manager = FindCameraManager(id);
            if (!_isLoggedIn || manager == null) return;
            GoLiveImpl(manager.Camera);
        }

        public void Pause(string id)
        {
            if (_isEdge) return;

            CameraManagerItem manager = FindCameraManager(id);
            if (!_isLoggedIn || manager == null) return;
            PauseImpl(manager.Camera);
        }

        public void Stop(string id)
        {
            CameraManagerItem manager = FindCameraManager(id);
            if (!_isLoggedIn || manager == null) return;
            _cameraManager.Remove(id);
            StopImpl(manager.Camera);
        }

        public void Logout()
        {
            if (!_isLoggedIn) return;
            List<string> keys = new List<string>(_cameraManager.Keys);
            foreach (string id in keys)
                Stop(id);
            _cameraManager.Clear();
            LogoutImpl();
            _isLoggedIn = false;
        }
        #endregion

        protected string GetRtspStream(ICamera camera)
        {
            string path = string.Empty;
            using (var sd = new StreamDiscoveryNs.StreamDiscovery())
            {
                sd.Url = string.Format("http://{0}:{1}/control/StreamDiscovery-{2}", camera.IP, camera.Port, camera.ServiceId);
                var urls = sd.StreamQuery();

                var foundUrl = urls.FirstOrDefault(url => url.transmission == "unicast" && url.stream.FirstOrDefault(id => id.type == "video") != null);
                if (foundUrl != null)
                    path = foundUrl.address;
            }
            return path;
        }

        /// <summary>
        /// Find the id associated with the display
        /// </summary>
        /// <param name="display"></param>
        /// <returns></returns>
        private string FindId(IWin32Window display)
        {
            string id = string.Empty;
            foreach (KeyValuePair<string, CameraManagerItem> entry in _cameraManager)
            {
                if (display.Handle == entry.Value.Display.Handle)
                {
                    id = entry.Key;
                    break;
                }
            }
            return id;
        }

        /// <summary>
        /// Find the camera manager item associated with a display
        /// </summary>
        /// <param name="display"></param>
        /// <returns></returns>
        private CameraManagerItem FindCameraManager(IWin32Window display)
        {
            CameraManagerItem manager = null;
            foreach (KeyValuePair<string, CameraManagerItem> entry in _cameraManager)
            {
                if (display.Handle == entry.Value.Display.Handle)
                {
                    manager = entry.Value;
                    break;
                }
            }
            return manager;
        }

        /// <summary>
        /// Find the camera manager item associated with the session id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private CameraManagerItem FindCameraManager(string id)
        {
            CameraManagerItem manager = null;
            if (id != null && _cameraManager.ContainsKey(id))
                manager = _cameraManager[id];
            return manager;
        }

        #region IDisposable Members
        protected virtual void Dispose(bool disposed)
        {
            Logout();
            _cameraManager.Clear();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
