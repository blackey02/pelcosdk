PelcoSdk
====================

Summary
---------------------
The PelcoSdk project is a single solution with many project samples demonstrating different features of the Pelco SDK.

Features
---------------------
* Samples are kept up to date and bugs are fixed and committed when found
* This solution contains projects that only use the SDK to solve use cases

Building
---------------------
1. git clone https://bitbucket.org/blackey02/pelcosdk.git
2. Open the Visual Studio solution file
3. Build and run

Acknowledgements
---------------------
* [Pelco](http://pdn.pelco.com)