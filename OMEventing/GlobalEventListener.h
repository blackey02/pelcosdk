#pragma once

namespace OMEventing {
	namespace Pelco {

		typedef void(*GlobalEventHandler)(void *listener, const PelcoSDK::Event &pelcoEvent);

		class GlobalEventListener : public PelcoSDK::GlobalEventListener {
		public:
			GlobalEventListener(void *listener, GlobalEventHandler handler);
			virtual ~GlobalEventListener();

		private:
			void *_listener;
			GlobalEventHandler _handler;
			PelcoSDK::EventSubscription _subscription;

		public:
			virtual void Handle(const PelcoSDK::Event &event);
		};
	}
}