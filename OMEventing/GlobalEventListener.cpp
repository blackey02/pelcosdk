#include "stdafx.h"
#include "GlobalEventListener.h"

using namespace OMEventing::Pelco;

GlobalEventListener::GlobalEventListener(void *listener, GlobalEventHandler handler) 
	: PelcoSDK::GlobalEventListener() {
	_subscription = PelcoSDK::Events::Subscribe(this);
	_listener = listener;
	_handler = handler;
}

GlobalEventListener::~GlobalEventListener() {
	PelcoSDK::Events::UnSubscribe(_subscription);
}

void GlobalEventListener::Handle(const PelcoSDK::Event &event) {
	if(_listener && _handler)
		_handler(_listener, event);
}