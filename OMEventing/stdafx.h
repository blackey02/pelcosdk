// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
#pragma once

#define DeletePtr(x) { if(x) { delete x; x = NULL; } }

#include <msclr/marshal_cppstd.h>
#include <sstream>

#include "PelcoSDK/PelcoSDK.h"
#include "PelcoSDK/SystemCollection.h"
#include "PelcoSDK/System.h"
#include "PelcoSDK/PString.h"
#include "PelcoSDK/Events.h"
#include "PelcoSDK/Event.h"
#include "PelcoSDK/Exception.h"