#include "stdafx.h"
#include "SystemEventListener.h"

using namespace OMEventing::Pelco;

SystemEventListener::SystemEventListener(PelcoSDK::System &system, void *listener, SystemEventHandler handler)
	: PelcoSDK::SystemEventListener(
		PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_DIAGNOSTIC) | PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_MOTION) | 
		PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_ONLINE) | PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_PHYSICAL) |
		PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_PROPERTY_CHANGED) | PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_SDK_STATE) |
		PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_STREAM) | PelcoSDK::Event::GetMask(PelcoSDK::Event::ET_VIDEO_ANALYTICS)), _system(system) {
	_subscription = _system.Subscribe(this);
	_listener = listener;
	_handler = handler;
}

SystemEventListener::~SystemEventListener() {
	_system.UnSubscribe(_subscription);
}

void SystemEventListener::Handle(const PelcoSDK::Event &event) {
	if(_handler && _listener)
		_handler(_listener, event);
}