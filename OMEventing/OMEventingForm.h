#pragma once

namespace OMEventing {

	namespace Pelco {
		class GlobalEventListener;
		class SystemEventListener;
	}

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class OMEventingForm : public System::Windows::Forms::Form
	{
	public:
		OMEventingForm();
		!OMEventingForm();
		virtual ~OMEventingForm();

	private:
		bool _sdkStarted;
		Pelco::GlobalEventListener *_globalListener;
		std::vector<Pelco::SystemEventListener*> *_systemListeners;
		void *_rawThis;

	protected:
		void StartSdk(Object^);
		void StartProgress(String ^statusInfo);
		void EndProgress();
		void UpdateStatus(String ^statusInfo);
		void RunAndSleep(int desiredSleepTime, MethodInvoker ^method);
		void EnableUi(bool);
		
	protected:
		std::string SmIp();
		int SmPort();
		std::string SmUsername();
		std::string SmPassword();

	protected:
		void OMEventingForm_FormClosing(Object ^sender, FormClosingEventArgs ^e);
		void ButtonAddSystem_Click(Object^ sender, EventArgs^ e);
		void ButtonAddSystemClickDel(Object^);
		void StartSdkDel();
		void LoadSystemsDel();
		void CloseForm(Object^);
		void CloseFormDel();
		void WriteEvent(std::string event);
		void WriteEvent(String ^event);

	public:
		void OnPelcoEvent(bool globalEvent, const PelcoSDK::Event &event);

#pragma region Windows Form Designer generated code

	private: 
		System::Windows::Forms::StatusStrip^  statusStripMain;
		System::Windows::Forms::TextBox^  textBoxMain;
		System::Windows::Forms::GroupBox^  groupBoxEvents;
		System::Windows::Forms::GroupBox^  groupBoxAddSystem;
		System::Windows::Forms::Label^  label4;
		System::Windows::Forms::TextBox^  textBoxSmPort;
		System::Windows::Forms::Label^  label3;
		System::Windows::Forms::TextBox^  textBoxSmIp;
		System::Windows::Forms::Label^  label2;
		System::Windows::Forms::TextBox^  textBoxSmPassword;
		System::Windows::Forms::Label^  label1;
		System::Windows::Forms::TextBox^  textBoxSmUsername;
		System::Windows::Forms::Button^  buttonAddSystem;
		System::Windows::Forms::ToolStripProgressBar^  toolStripProgressBarMain;
		System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabelMain;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(OMEventingForm::typeid));
			this->statusStripMain = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripProgressBarMain = (gcnew System::Windows::Forms::ToolStripProgressBar());
			this->toolStripStatusLabelMain = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->textBoxMain = (gcnew System::Windows::Forms::TextBox());
			this->groupBoxEvents = (gcnew System::Windows::Forms::GroupBox());
			this->groupBoxAddSystem = (gcnew System::Windows::Forms::GroupBox());
			this->buttonAddSystem = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->textBoxSmPort = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBoxSmIp = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBoxSmPassword = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBoxSmUsername = (gcnew System::Windows::Forms::TextBox());
			this->statusStripMain->SuspendLayout();
			this->groupBoxEvents->SuspendLayout();
			this->groupBoxAddSystem->SuspendLayout();
			this->SuspendLayout();
			// 
			// statusStripMain
			// 
			this->statusStripMain->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripProgressBarMain, 
				this->toolStripStatusLabelMain});
			this->statusStripMain->Location = System::Drawing::Point(0, 365);
			this->statusStripMain->Name = L"statusStripMain";
			this->statusStripMain->Size = System::Drawing::Size(557, 22);
			this->statusStripMain->TabIndex = 0;
			this->statusStripMain->Text = L"statusStrip1";
			// 
			// toolStripProgressBarMain
			// 
			this->toolStripProgressBarMain->Name = L"toolStripProgressBarMain";
			this->toolStripProgressBarMain->Size = System::Drawing::Size(100, 16);
			this->toolStripProgressBarMain->Style = System::Windows::Forms::ProgressBarStyle::Marquee;
			// 
			// toolStripStatusLabelMain
			// 
			this->toolStripStatusLabelMain->Name = L"toolStripStatusLabelMain";
			this->toolStripStatusLabelMain->Size = System::Drawing::Size(118, 17);
			this->toolStripStatusLabelMain->Text = L"toolStripStatusLabel1";
			// 
			// textBoxMain
			// 
			this->textBoxMain->Cursor = System::Windows::Forms::Cursors::Default;
			this->textBoxMain->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxMain->Location = System::Drawing::Point(3, 16);
			this->textBoxMain->Multiline = true;
			this->textBoxMain->Name = L"textBoxMain";
			this->textBoxMain->ReadOnly = true;
			this->textBoxMain->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->textBoxMain->Size = System::Drawing::Size(527, 240);
			this->textBoxMain->TabIndex = 1;
			// 
			// groupBoxEvents
			// 
			this->groupBoxEvents->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxEvents->Controls->Add(this->textBoxMain);
			this->groupBoxEvents->Location = System::Drawing::Point(12, 103);
			this->groupBoxEvents->Name = L"groupBoxEvents";
			this->groupBoxEvents->Size = System::Drawing::Size(533, 259);
			this->groupBoxEvents->TabIndex = 3;
			this->groupBoxEvents->TabStop = false;
			this->groupBoxEvents->Text = L"Events";
			// 
			// groupBoxAddSystem
			// 
			this->groupBoxAddSystem->Controls->Add(this->buttonAddSystem);
			this->groupBoxAddSystem->Controls->Add(this->label4);
			this->groupBoxAddSystem->Controls->Add(this->textBoxSmPort);
			this->groupBoxAddSystem->Controls->Add(this->label3);
			this->groupBoxAddSystem->Controls->Add(this->textBoxSmIp);
			this->groupBoxAddSystem->Controls->Add(this->label2);
			this->groupBoxAddSystem->Controls->Add(this->textBoxSmPassword);
			this->groupBoxAddSystem->Controls->Add(this->label1);
			this->groupBoxAddSystem->Controls->Add(this->textBoxSmUsername);
			this->groupBoxAddSystem->Location = System::Drawing::Point(12, 12);
			this->groupBoxAddSystem->Name = L"groupBoxAddSystem";
			this->groupBoxAddSystem->Size = System::Drawing::Size(533, 85);
			this->groupBoxAddSystem->TabIndex = 4;
			this->groupBoxAddSystem->TabStop = false;
			this->groupBoxAddSystem->Text = L"Add System";
			// 
			// buttonAddSystem
			// 
			this->buttonAddSystem->Enabled = false;
			this->buttonAddSystem->Location = System::Drawing::Point(436, 23);
			this->buttonAddSystem->Name = L"buttonAddSystem";
			this->buttonAddSystem->Size = System::Drawing::Size(78, 46);
			this->buttonAddSystem->TabIndex = 10;
			this->buttonAddSystem->Text = L"Add";
			this->buttonAddSystem->UseVisualStyleBackColor = true;
			this->buttonAddSystem->Click += gcnew System::EventHandler(this, &OMEventingForm::ButtonAddSystem_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(220, 52);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(45, 13);
			this->label4->TabIndex = 9;
			this->label4->Text = L"SM Port";
			// 
			// textBoxSmPort
			// 
			this->textBoxSmPort->Location = System::Drawing::Point(271, 49);
			this->textBoxSmPort->Name = L"textBoxSmPort";
			this->textBoxSmPort->Size = System::Drawing::Size(118, 20);
			this->textBoxSmPort->TabIndex = 8;
			this->textBoxSmPort->Text = L"60001";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(220, 26);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(36, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"SM IP";
			// 
			// textBoxSmIp
			// 
			this->textBoxSmIp->Location = System::Drawing::Point(271, 23);
			this->textBoxSmIp->Name = L"textBoxSmIp";
			this->textBoxSmIp->Size = System::Drawing::Size(118, 20);
			this->textBoxSmIp->TabIndex = 6;
			this->textBoxSmIp->Text = L"192.168.5.10";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 52);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(53, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"Password";
			// 
			// textBoxSmPassword
			// 
			this->textBoxSmPassword->Location = System::Drawing::Point(67, 49);
			this->textBoxSmPassword->Name = L"textBoxSmPassword";
			this->textBoxSmPassword->Size = System::Drawing::Size(118, 20);
			this->textBoxSmPassword->TabIndex = 4;
			this->textBoxSmPassword->Text = L"admin";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 26);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(55, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Username";
			// 
			// textBoxSmUsername
			// 
			this->textBoxSmUsername->Location = System::Drawing::Point(67, 23);
			this->textBoxSmUsername->Name = L"textBoxSmUsername";
			this->textBoxSmUsername->Size = System::Drawing::Size(118, 20);
			this->textBoxSmUsername->TabIndex = 1;
			this->textBoxSmUsername->Text = L"admin";
			// 
			// OMEventingForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(557, 387);
			this->Controls->Add(this->groupBoxAddSystem);
			this->Controls->Add(this->groupBoxEvents);
			this->Controls->Add(this->statusStripMain);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->Name = L"OMEventingForm";
			this->Text = L"OM Eventing";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &OMEventingForm::OMEventingForm_FormClosing);
			this->statusStripMain->ResumeLayout(false);
			this->statusStripMain->PerformLayout();
			this->groupBoxEvents->ResumeLayout(false);
			this->groupBoxEvents->PerformLayout();
			this->groupBoxAddSystem->ResumeLayout(false);
			this->groupBoxAddSystem->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}

