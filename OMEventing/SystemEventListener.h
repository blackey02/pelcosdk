#pragma once

namespace OMEventing {
	namespace Pelco {

		typedef void(*SystemEventHandler)(void *listener, const PelcoSDK::Event &pelcoEvent);

		class SystemEventListener : public PelcoSDK::SystemEventListener {
		public:
			SystemEventListener(PelcoSDK::System &system, void *listener, SystemEventHandler handler);
			virtual ~SystemEventListener();

		private:
			void *_listener;
			SystemEventHandler _handler;
			PelcoSDK::System _system;
			PelcoSDK::EventSubscription _subscription;

		public:
			virtual void Handle(const PelcoSDK::Event &event);
		};
	}
}