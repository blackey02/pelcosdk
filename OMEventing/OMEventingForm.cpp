#include "stdafx.h"
#include "OMEventingForm.h"
#include "GlobalEventListener.h"
#include "SystemEventListener.h"

using namespace System::Diagnostics;
using namespace System::Threading;
using namespace System::Runtime::InteropServices;
using namespace OMEventing;

void OnPelcoEventPrivate(void *listener, const PelcoSDK::Event &pelcoEvent);

OMEventingForm::OMEventingForm() {
	InitializeComponent();
	_rawThis = reinterpret_cast<void*>(GCHandle::ToIntPtr(GCHandle::Alloc(this)).ToPointer());
	MinimumSize = Size;
	toolStripProgressBarMain->Visible = false;
	toolStripStatusLabelMain->Visible = false;
	toolStripStatusLabelMain->Text = String::Empty;
	ThreadPool::QueueUserWorkItem(gcnew WaitCallback(this, &OMEventingForm::StartSdk));
}

OMEventingForm::~OMEventingForm() {
	this->!OMEventingForm();
}

OMEventingForm::!OMEventingForm() {
}

void OMEventingForm::StartSdk(System::Object^) {
	_sdkStarted = true;
	StartProgress("Starting Pelco SDK");
	MethodInvoker ^method = gcnew MethodInvoker(this, &OMEventingForm::StartSdkDel);
	RunAndSleep(2000, method);

	_globalListener = new Pelco::GlobalEventListener(_rawThis, &OnPelcoEventPrivate);
	WriteEvent("Successfully subscribed to global events");
	_systemListeners = new std::vector<Pelco::SystemEventListener*>();

	UpdateStatus("Loading Systems");
	method = gcnew MethodInvoker(this, &OMEventingForm::LoadSystemsDel);
	RunAndSleep(1000, method);
	EnableUi(true);
	EndProgress();
}

void OnPelcoEventPrivate(void *listener, const PelcoSDK::Event &pelcoEvent) {
	GCHandle h = GCHandle::FromIntPtr(IntPtr(reinterpret_cast<void*>(listener)));
	OMEventingForm^ form = safe_cast<OMEventingForm^>(h.Target);
	form->OnPelcoEvent(pelcoEvent.IsGlobal(), pelcoEvent);
}

void OMEventingForm::OnPelcoEvent(bool globalEvent, const PelcoSDK::Event &event) {
	PelcoSDK::Event::TYPE eventType = event.GetType();
	String ^type = gcnew String(event.Info().c_str());
	String ^uuid = gcnew String(event.GetDeviceUuid().c_str());
	unsigned systemId = event.GetSystemId();
	DateTime time(1970,1,1,0,0,0,0);
	time = time.AddSeconds(Convert::ToDouble(event.GetUTCTime()));
	DateTime::SpecifyKind(time, System::DateTimeKind::Utc);
	time = time.ToLocalTime();
	String ^info = String::Empty;

	if(eventType == PelcoSDK::Event::ET_DIAGNOSTIC) {}
	else if(eventType == PelcoSDK::Event::ET_MOTION) {}
	else if(eventType == PelcoSDK::Event::ET_ONLINE) 
		info = ((PelcoSDK::OnlineEvent*)&event)->on ? "Online" : "Offline";
	else if(eventType == PelcoSDK::Event::ET_PHYSICAL) {}
	else if(eventType == PelcoSDK::Event::ET_PROPERTY_CHANGED) { 
		PelcoSDK::PropertyChangedEvent *evt = (PelcoSDK::PropertyChangedEvent*)&event;
		info = String::Format("Name {0} | Value {1}", gcnew String(evt->propertyName.c_str()), gcnew String(evt->propertyValue.c_str()));
	}
	else if(eventType == PelcoSDK::Event::ET_SDK_STATE) {
		PelcoSDK::StateChangedEvent *evt = (PelcoSDK::StateChangedEvent*)&event;
		String ^state = String::Empty;
		if(evt->state == PelcoSDK::StateChangedEvent::COLLECTION_STARTED) state = "COLLECTION_STARTED";
		else if(evt->state == PelcoSDK::StateChangedEvent::COLLECTION_COMPLETED) state = "COLLECTION_COMPLETED";
		else if(evt->state == PelcoSDK::StateChangedEvent::COLLECTION_FAILED) state = "COLLECTION_FAILED";
		else if(evt->state == PelcoSDK::StateChangedEvent::COLLECTION_REMOVED) state = "COLLECTION_REMOVED";
		info = String::Format("{0} {1}", state, gcnew String(evt->extraInfo.c_str()));
	}
	else if(eventType == PelcoSDK::Event::ET_STREAM) {
		PelcoSDK::StreamEvent *evt = (PelcoSDK::StreamEvent*)&event;
		String ^kind = String::Empty;
		if(evt->kind == PelcoSDK::StreamEvent::CONNECTION_FAILED) kind = "CONNECTION_FAILED";
		else if(evt->kind == PelcoSDK::StreamEvent::MULTICAST_CONNECTION_FAILED) kind = "MULTICAST_CONNECTION_FAILED";
		else if(evt->kind == PelcoSDK::StreamEvent::PLAY_FAILED) kind = "PLAY_FAILED";
		else if(evt->kind == PelcoSDK::StreamEvent::STOPPED_RECEIVING_PACKETS) kind = "STOPPED_RECEIVING_PACKETS";
		else if(evt->kind == PelcoSDK::StreamEvent::UNICAST_CONNECTION_FAILED) kind = "UNICAST_CONNECTION_FAILED";
		info = String::Format("{0} {1}", kind, gcnew String(evt->extraInfo.c_str()));
	}
	else if(eventType == PelcoSDK::Event::ET_VIDEO_ANALYTICS) {}

	String ^pelcoEventType = globalEvent ? "GlobalEvent" : "SystemEvent";
	String ^log = String::Format("{0} - Type: {1} / Time: {2} / UUID: {3} / Msg: {4}", pelcoEventType, type, time, uuid, info);
	WriteEvent(log);
}

void OMEventingForm::EnableUi(bool enable) {
	if (InvokeRequired) {
		BeginInvoke(gcnew Action<bool>(this, &OMEventingForm::EnableUi), gcnew array<Object^> { enable });
		return;
	}

	buttonAddSystem->Enabled = enable;
}

void OMEventingForm::StartSdkDel() {
	PelcoSDK::Startup();
}

void OMEventingForm::LoadSystemsDel() {
	std::string stdSmUsername = SmUsername();
	std::string stdSmPassword = SmPassword();

	PelcoSDK::SystemCollection sysCollection;
	for(unsigned i = 0; i < sysCollection.GetCount(); i++) {
		PelcoSDK::System system = sysCollection.GetItem(i);
		bool loggedIn = system.Login(stdSmUsername, stdSmPassword);
		if(loggedIn) {
			Pelco::SystemEventListener *listener = new Pelco::SystemEventListener(system, _rawThis, &OnPelcoEventPrivate);
			_systemListeners->push_back(listener);
			WriteEvent("Successfully subscribed to system: " + std::string(system.GetIp().c_str()));
		}
		else
			WriteEvent("Unable to subscribe to events from the following system because the credentials were invalid. (" + std::string(system.GetIp().c_str()) + ")");
	}
}

void OMEventingForm::StartProgress(String ^statusInfo) {
	if (InvokeRequired) {
		BeginInvoke(gcnew Action<String^>(this, &OMEventingForm::StartProgress), gcnew array<Object^> { statusInfo });
		return;
	}

	toolStripStatusLabelMain->Text = statusInfo;
	toolStripStatusLabelMain->Visible = true;
	toolStripProgressBarMain->Visible = true;
}

void OMEventingForm::EndProgress() {
	if (InvokeRequired) {
		BeginInvoke(gcnew MethodInvoker(this, &OMEventingForm::EndProgress));
		return;
	}

	toolStripProgressBarMain->Visible = false;
	toolStripStatusLabelMain->Visible = false;
	toolStripStatusLabelMain->Text = String::Empty;
}

void OMEventingForm::UpdateStatus(String ^statusInfo) {
	if (InvokeRequired) {
		BeginInvoke(gcnew Action<String^>(this, &OMEventingForm::UpdateStatus), gcnew array<Object^> { statusInfo });
		return;
	}

	bool isVisible = !String::IsNullOrWhiteSpace(statusInfo);
	toolStripStatusLabelMain->Visible = isVisible;
	toolStripStatusLabelMain->Text = isVisible ? statusInfo : String::Empty;
}

void OMEventingForm::RunAndSleep(int desiredSleepTime, MethodInvoker ^method) {
	Stopwatch ^timer = gcnew Stopwatch();
	timer->Start();
	method();
	timer->Stop();
	int sleepTime = Math::Max(0, desiredSleepTime - (int)timer->ElapsedMilliseconds);
	Thread::Sleep(sleepTime);
}

void OMEventingForm::OMEventingForm_FormClosing(Object ^sender, FormClosingEventArgs ^e) {
	if (_sdkStarted) {
		WaitCallback ^method = gcnew WaitCallback(this, &OMEventingForm::CloseForm);
		ThreadPool::QueueUserWorkItem(method);
		e->Cancel = true;
	}
}

void OMEventingForm::CloseForm(Object^) {
	if(!_globalListener || !_systemListeners)
		return;

	DeletePtr(_globalListener);
	WriteEvent("Successfully unsubscribed from global events");

	for(unsigned i = 0; i < _systemListeners->size(); i++) {
		DeletePtr(_systemListeners->at(i));
		WriteEvent("Successfully unsubscribed from a system");
	}

	_systemListeners->clear();
	DeletePtr(_systemListeners);
	
	MethodInvoker ^method = gcnew MethodInvoker(this, &OMEventingForm::CloseFormDel);
	StartProgress("Stopping Pelco SDK");
	RunAndSleep(1000, method);

	_sdkStarted = false;
	if (InvokeRequired)
		BeginInvoke(gcnew MethodInvoker(this, &Form::Close));
	else
		Close();
}

void OMEventingForm::CloseFormDel() {
	PelcoSDK::Shutdown();
}

void OMEventingForm::ButtonAddSystem_Click(Object^ sender, EventArgs^ e) {
	WaitCallback ^method = gcnew WaitCallback(this, &OMEventingForm::ButtonAddSystemClickDel);
	ThreadPool::QueueUserWorkItem(method);
}

void OMEventingForm::ButtonAddSystemClickDel(Object^) {
	EnableUi(false);
	StartProgress("Adding new system");
	bool found = false;
	std::string stdSmIp = SmIp();
	int stdSmPort = SmPort();
	std::string stdSmUsername = SmUsername();
	std::string stdSmPassword = SmPassword();

	std::string msg = "";
	PelcoSDK::SystemCollection systemCollection;
	for(unsigned i = 0; i < systemCollection.GetCount(); i++) {
		PelcoSDK::System system = systemCollection.GetItem(i);

		bool isSameSystem = true;
		isSameSystem = isSameSystem && std::string(system.GetIp().c_str()) == stdSmIp;
		isSameSystem = isSameSystem && system.GetPort() == stdSmPort;

		if(isSameSystem) {
			msg = "Unable to add system since it already exists";
			break;
		}
	}

	if(msg == "") {
			std::stringstream ss;
			ss << stdSmUsername << ":" << stdSmPassword << "@";
			ss << "pelcosystem://" << stdSmIp << ":" << stdSmPort;
			std::string scheme = ss.str();
			try {
			PelcoSDK::System newSystem(scheme);

			Pelco::SystemEventListener *listener = new Pelco::SystemEventListener(newSystem, _rawThis, &OnPelcoEventPrivate);
			_systemListeners->push_back(listener);
			msg = "Successfully subscribed to system: " + std::string(newSystem.GetIp().c_str());
		}
		catch(PelcoSDK::Exception e) {
			msg = "Unable to add the specified system to the collection. " + std::string(e.Message().c_str());
		}
	}

	WriteEvent(msg);
	EndProgress();
	EnableUi(true);
}

void OMEventingForm::WriteEvent(std::string event) {
	String ^cliEvent = gcnew String(event.c_str());
	if (InvokeRequired) {
		BeginInvoke(gcnew Action<String^>(this, &OMEventingForm::WriteEvent), gcnew array<Object^> { cliEvent });
		return;
	}

	WriteEvent(cliEvent);
}

void OMEventingForm::WriteEvent(String ^event) {
	if (InvokeRequired) {
		BeginInvoke(gcnew Action<String^>(this, &OMEventingForm::WriteEvent), gcnew array<Object^> { event });
		return;
	}

	event = String::Format("{0} - {1}{2}", DateTime::Now, event, Environment::NewLine);
	textBoxMain->AppendText(event);
}

std::string OMEventingForm::SmIp() {
	String ^smIp = textBoxSmIp->Text->Trim();
	msclr::interop::marshal_context cxt;
	std::string stdSmIp = cxt.marshal_as<std::string>(smIp);
	return stdSmIp;
}

int OMEventingForm::SmPort() {
	String ^smPort = textBoxSmPort->Text->Trim();
	int stdSmPort = Convert::ToInt32(smPort);
	return stdSmPort;
}

std::string OMEventingForm::SmUsername() {
	String ^smUsername = textBoxSmUsername->Text->Trim();
	msclr::interop::marshal_context cxt;
	std::string stdSmUsername = cxt.marshal_as<std::string>(smUsername);
	return stdSmUsername;
}

std::string OMEventingForm::SmPassword() {
	String ^smPassword = textBoxSmPassword->Text->Trim();
	msclr::interop::marshal_context cxt;
	std::string stdSmPassword = cxt.marshal_as<std::string>(smPassword);
	return stdSmPassword;
}